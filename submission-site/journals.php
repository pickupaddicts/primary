<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
		<script type="text/javascript">
			var fixed = false;
			document.onscroll = function() {
				if(document.body.scrollTop >= 250) {
					if(!fixed) {
						fixed = true;
						var nav = document.getElementById("navigation").style;
						nav.position = "fixed";
						nav.top = "0px";
						document.getElementById("content").setAttribute("style", "margin-top: 135px");
					}
				}
				else {
					if(fixed) {
						fixed = false;
						var nav = document.getElementById("navigation").style;
						nav.position = "relative";
						nav.top = "";
						document.getElementById("content").setAttribute("style", "margin-top: 0px");
					}
				}
			}
		</script>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Journal Submissions</h1>
					<p>
						Everyone must maintain a journal showing: date, start time, end time, total time spent, task description for all times spent on this project. Remember to include times that we take for granted such as responding to emails and meeting times. Finally, keep track of the total accumulated time spent on the project.
						<br />
						<br />
						Grading: Your journal is graded on thoroughness. You should have a journal entry for every time block you worked on the project. The task descriptions should be detailed enough to be meaningful, but they don't have to be exhaustive. For example, "worked on the project" is too broad. "Debugged the new algorithm for calculating user suggestions" seems about right. One possibility is to use your commit messages from your repository.
					</p>
					<hr />
					<br />
					<h2>December 10, 2014</h2>
					<a href="files/journal-brian1210.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Brian Johnson Submission</a>
					<br />
					<a href="files/journal-nick1210.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;" style="display: inline-block; margin-bottom: 5px;">Nicholas Olesak Submission</a>
					<br />
					<a href="files/journal-cody1210.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Cody Rivers Submission</a>
					<br />
					<a href="files/journal-josh1210.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Joshua Spicer Submission</a>
					<br />
					<h2>November 17, 2014</h2>
					<a href="files/journal-brian1117.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Brian Johnson Submission</a>
					<br />
					<a href="files/journal-nick1117.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;" style="display: inline-block; margin-bottom: 5px;">Nicholas Olesak Submission</a>
					<br />
					<a href="files/journal-cody1117.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Cody Rivers Submission</a>
					<br />
					<a href="files/journal-josh1117.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Joshua Spicer Submission</a>
					<br />
					<h2>October 27, 2014</h2>
					<a href="files/journal-brian1027.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Brian Johnson Submission</a>
					<br />
					<a href="files/journal-nick1027.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;" style="display: inline-block; margin-bottom: 5px;">Nicholas Olesak Submission</a>
					<br />
					<a href="files/journal-cody1027.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Cody Rivers Submission</a>
					<br />
					<a href="files/journal-josh1027.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Joshua Spicer Submission</a>
					<br />
					<h2>October 6, 2014</h2>
					<a href="files/journal-brian106.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Brian Johnson Submission</a>
					<br />
					<a href="files/journal-nick106.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;" style="display: inline-block; margin-bottom: 5px;">Nicholas Olesak Submission</a>
					<br />
					<a href="files/journal-cody106.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Cody Rivers Submission</a>
					<br />
					<a href="files/journal-josh106.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Joshua Spicer Submission</a>
					<br />
					<h2>September 10, 2014</h2>
					<a href="files/journal-brian.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Brian Johnson Submission</a>
					<br />
					<a href="files/journal-nick.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;" style="display: inline-block; margin-bottom: 5px;">Nicholas Olesak Submission</a>
					<br />
					<a href="files/journal-cody.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Cody Rivers Submission</a>
					<br />
					<a href="files/journal-josh.pdf" target="_blank" style="display: inline-block; margin-bottom: 5px;">Joshua Spicer Submission</a>
					<br />
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>