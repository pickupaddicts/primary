<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>About Pickup Addicts</h1>
					<hr />
					<br />
					<h2>What Is It?</h2>
					<p>
						The Pickup Addict app is a comprehensive web application to help you find pickup sports games near you. Pickup Addict lets you search for any sport you're looking for and connects you with others that are looking to play. Compete recreationally, just for fun, or competitively and track your stats and game time. Either way, the Pickup Addict app gets your out there doing what you love.
					</p>
					<br />
					<h2>Features</h2>
					<ul>
						<li>Select the sport that you are interested in</li>
						<li>Post a new pick up game that you customize</li>
						<li>Join an existing pick up game someone else created</li>
						<li>Track your stats and ranking in real-time online</li>
						<li>Find game locations near you and find them on the map</li>
						<li>Post pick up game details and locations to social media</li>
					</ul>
					<br />
					<h2>Get It</h2>
					<p>
						The Pickup Addict app is available for:
					</p>
					<ul>
						<li>iOS devices in the app store</li>
						<li>Android devices on Google Play</li>
					</ul>
					<p>
						You can also log in to your account on through your web browser to check stats and game details from your laptop or PC.
					</p>
					<br />
					<div style="float: left;">
						<a href="files/prospectus.pdf" target="_blank" class="btn btn-blue" style="width: 350px;">Click here to see the full prospectus</a>
						<br />
						<a href="files/feasability-study.pdf" target="_blank" class="btn btn-red" style="width: 350px;">Click here to see the feasibility study</a>
						<br />
						<a href="files/final-report.pdf" target="_blank" class="btn btn-green" style="width: 350px;">Click here to see the final report</a>
					</div>
					<div style="float: right;">
						<a href="files/scrum1.pdf" target="_blank" class="btn btn-green">Click here to see the scrum report #1</a>
						<br />
						<a href="files/scrum2.pdf" target="_blank" class="btn btn-green">Click here to see the scrum report #2</a>
						<br />
						<a href="files/scrum3.pdf" target="_blank" class="btn btn-green">Click here to see the scrum report #3</a>
						<br />
						<a href="files/scrum4.pdf" target="_blank" class="btn btn-green">Click here to see the scrum report #4</a>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>