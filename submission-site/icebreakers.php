<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Icebreakers</h1>
					<p>
						To get the ball rolling, each team member should introduce themselves to the rest of the team via a small assignment. Each team member should create an introduction with your name, contact information, a list of technical strengths (areas where you particularly excel or are interested in learning more), technical weaknesses (areas that you would rather not focus your attention), and personal interests (just for fun). 
						<br />
						<br />
						Each team member should share their introduction with everyone else in the team. If you share this via email, please cc the instructor. Otherwise, you may simply post your icebreaker on your team website.
					</p>
					<hr />
					<br />
					<div style="float: left; width: 232px;">
						<p style="margin-bottom: 100px; text-align: center; font-size: 22px;">
							<a href="files/brian.pdf" target="_blank">
								<img src="images/brian.png" style="width: 232px; height: 300px; border: 5px solid #292929; border-radius: 5px;" />
							</a>
							Brian Johnson
						</p>
						<p style="text-align: center; font-size: 22px;">
							<a href="files/cody.pdf" target="_blank">
								<img src="images/cody.png" style="width: 232px; height: 300px; border: 5px solid #292929; border-radius: 5px;" />
							</a>
							Cody Rivers
						</p>
					</div>
					<div style="float: right; width: 232px; text-align: center;">
						<p style="margin-bottom: 100px; font-size: 22px;">
							<a href="files/nick.pdf" target="_blank">
								<img src="images/nick.png" style="width: 232px; height: 300px; border: 5px solid #292929; border-radius: 5px;" />
							</a>
							Nicholas Olesak
						</p>
						<p style="text-align: center; font-size: 22px;">
							<a href="files/josh.pdf" target="_blank">
								<img src="images/nick.png" style="width: 232px; height: 300px; border: 5px solid #292929; border-radius: 5px;" />
							</a>
							Joshua Spicer
						</p>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>