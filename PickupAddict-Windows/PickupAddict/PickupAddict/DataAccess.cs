﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PickupAddict
{
    public class DataAccess
    {
        private const string BASE_ADDRESS = "http://pickupaddict.com/api/?";

        public static JObject UserLogin(string email, string password)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=user&action=login"
                + AddParam("email", email)
                + AddParam("password", password));
        }


        public static JObject UserCreate(string email, string first, string last, string password)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=user&action=create"
                + AddParam("email", email)
                + AddParam("password", password)
                + AddParam("firstName", first)
                + AddParam("lastName", last));
        }

        public static JObject GetAllSports()
        {
            return ParseHttpIntoJObject("http://pickupaddict.com/api/?controller=sport&action=show");
        }


        public static JObject CreateGame(Game game)
        {
            return CreateGame(game.Location.LocationId, game.Sport, game.Name, game.Description, game.Start,
                        game.End, game.MinPlayers, game.MaxPlayers);
        }


        public static JObject CreateGame(int locId, Sport sport, String title, String desc, DateTime start, DateTime end, int minPlayers, int maxPlayers)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=game&action=create"
                + AddParam("loc", locId)
                + AddParam("sId", sport)
                + AddParam("title", title)
                + AddParam("des", desc)
                + AddParam("sTime", start)
                + AddParam("eTime", end)
                + AddParam("minP", minPlayers)
                + AddParam("maxP", maxPlayers));
        }


        public static JObject DeleteGame(Game game, String email)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=gameUser&action=create"
                + AddParam("gameId", game) // this isn't in the API YET!!!!!
                + AddParam("userId", email)
                + AddParam("entry", DateTime.Now));
        }



        public static JObject DeleteGame(Game game)
        {
            return ParseHttpIntoJObject("http://pickupaddict/api/?controller=game&action=delete"
                + AddParam("gameId", game.GameId));
        }


        public static JObject GetAllGames()
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=game&action=find");
            //return GetGames(null, null, null, null);//Calendar.Instance().Time());
        }


        public static JObject GetGames(String loc, Sport sport, String title, DateTime start)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=game&action=find"
                + AddParam("loc", loc)
                + AddParam("sId", sport) + AddParam("title", title)
                + AddParam("sTime", start));
        }

        public static JObject AddPlayerToGame(Game game, String email)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=gameUser&action=create"
                + AddParam("gameId", game.GameId)
                + AddParam("userId", email)
                + AddParam("entry", DateTime.Now));
        }

        public static JObject FindPlayersForGame(Game game)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=gameUser&action=find&gameId=" + game.GameId);
        }


        public static JObject FindGamesForPlayer(String email)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=gameUser&action=find&userId=" + email);
        }


        public static JObject RemovePlayerFromGame(Game game, String email)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=gameUser&action=delete"
                + AddParam("gameId", game.GameId)
                + AddParam("userId", email));
        }

        public static JObject CreateLocation(double lat, double lng, String desc)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=location&action=create"
                + AddParam("lat", lat)
                + AddParam("long", lng)
                + AddParam("des", desc));
        }

        /*
        public static Location CreateLocationWithReturn(String address)
        {
            return (Location) CreateFromJSON(CreateLocation(address), Location.class);
        }
	
    
        public static Object CreateFromJSON(JObject json, Class<?> t)
        {
            if (t.getClass() == Game.class.getClass()) 		return new Game(json);
            if (t.getClass() == Location.class.getClass()) 	return new Location(json);
            if (t.getClass() == Sport.class.getClass()) 	return new Sport(json);
		
            return null;
        }*/


        public static JObject GetAllLocations()
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=location&actions=create");
        }

        public static JObject GetLocation(int locId)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=location&actions=create"
                + AddParam("locID", locId));
        }


        public static JObject CreateLocation(String address)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=location&action=create"
                + AddParam("address", address));
        }

        public static JObject CreateLocation(int number, String street, String city, String state, int zip, String desc)
        {
            return ParseHttpIntoJObject(BASE_ADDRESS + "controller=location&action=create"
                + AddParam("hNum", number)
                + AddParam("address", street)
                + AddParam("city", city)
                + AddParam("state", state)
                + AddParam("zip", zip)
                + AddParam("des", desc));
        }

        public static JObject CreateLocationAndGame(Game game, double lat, double lng, string desc)
        {
            var loc = CreateLocation(lat, lng, desc);
            if ((bool)loc.SelectToken("success"))
            {
                var data = loc.SelectToken("data") as JObject;
                game.Location = new Location()
                {
                    LocationId = (int) data.SelectToken("locationId"),
                    Latitude = lat,
                    Longitude = lng,
                    Description = desc
                };
                return CreateGame(game);
            }
            return null; // an error occured
        }


        public static void SaveToApplicationDataContainer(string paramName, string value)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings =
                Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values[paramName] = value;
        }


        public static string LoadFromApplicationDataContainer(string paramName)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings =
                Windows.Storage.ApplicationData.Current.RoamingSettings;
            return (string) roamingSettings.Values[paramName];
        }


        private static string AddParam(string paramName, string value)
        {
            if (value != null && !string.IsNullOrWhiteSpace(value))
                return "&" + paramName + "=" + value.Replace(' ', '+');
            else
                return string.Empty;
        }


        private static string AddParam(string paramName, object value)
        {
            if (value != null)
                return "&" + paramName + "=" + value;
            else
                return string.Empty;
        }


        private static string AddParam(string paramName, int value)
        {
            if (value > 0)
                return "&" + paramName + "=" + value;
            else
                return string.Empty;
        }


        private static string AddParam(string paramName, DateTime value)
        {
            if (value != null)
                return "&" + paramName + "=" + value.ToString("yyyy-MM-dd+HH:mm:ss");
            else
                return string.Empty;
        }


        private static JObject ParseHttpIntoJObject(string url)
        {
            HttpClient client = new HttpClient();
            var result = client.GetStringAsync(url);
            result.Wait();
            return JObject.Parse(result.Result);
        }
    }
}