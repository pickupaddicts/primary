﻿using PickupAddict.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Popups;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls.Maps;
using Windows.Devices.Geolocation;
using Windows.Services.Maps;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using Newtonsoft.Json.Linq;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace PickupAddict
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewMap : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private Geolocator geoLocator;

        public ViewMap()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

        }

        
        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            Game game = null;
            try
            {
                game = e.Parameter as Game;
                MapIcon mapIcon = new MapIcon();
                // Locate your MapIcon
                //mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/my-position.png"));  
                // Show above the MapIcon  
                mapIcon.Title = game.Name;
                // Setting up MapIcon location  
                mapIcon.Location = new Geopoint(game.Location.Geoposition);
                // Positon of the MapIcon  
                mapIcon.NormalizedAnchorPoint = new Point(0.5, 0.5);
                MapControl.MapElements.Add(mapIcon);
                // Showing in the Map  
                await MapControl.TrySetViewAsync(mapIcon.Location, 12D, 0, 0, MapAnimationKind.Bow);
                return;
            }
            catch (Exception ex) { }

            geoLocator = new Geolocator();
            geoLocator.DesiredAccuracyInMeters = 10;

            try
            {
                game = e.Parameter as Game;

                // Getting Current Location  
                Geoposition geoposition = await geoLocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10));


                // Create a circle
                Ellipse fence = new Ellipse();
                fence.Width = 20;
                fence.Height = 20;
                fence.Stroke = new SolidColorBrush(Colors.Blue);
                fence.StrokeThickness = 10;
                fence.Opacity = .75;
                MapControl.SetLocation(fence, geoposition.Coordinate.Point);
                MapControl.SetNormalizedAnchorPoint(fence, new Point(0.5, 0.5));
                MapControl.Children.Add(fence);
                await MapControl.TrySetViewAsync(geoposition.Coordinate.Point, 12D, 0, 0, MapAnimationKind.Bow);
            }
            catch (UnauthorizedAccessException)
            {
                var md = new MessageDialog("Location service is turned off!");
                var x = md.ShowAsync();
            }
            this.navigationHelper.OnNavigatedTo(e);
        }



        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }


        private void AddMapIcon(Game game)
        {
            MapControl.MapElements.Add(new MapIcon()
            {
                Location = new Geopoint(game.Location.Geoposition),
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                Title = game.Name,
            });
        }

        #endregion
    }
}
