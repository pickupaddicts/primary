﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace PickupAddict
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static User User { get; private set; }

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            try
            {
                User = (User) e.Parameter;
                if (User == null)
                {
                    User = new User();
                    User.Email = DataAccess.LoadFromApplicationDataContainer("email");
                    User.FirstName = DataAccess.LoadFromApplicationDataContainer("firstName");
                    User.LastName = DataAccess.LoadFromApplicationDataContainer("lastName");
                }
                else
                {
                    DataAccess.SaveToApplicationDataContainer("email", User.Email);
                    DataAccess.SaveToApplicationDataContainer("firstName", User.FirstName);
                    DataAccess.SaveToApplicationDataContainer("lastName", User.LastName);
                }
            }
            catch (Exception ex) 
            {

            }

            RetrieveGamesForUser();
        }


        private void RetrieveGamesForUser()
        {
            JObject json = null;
            List<Game> games = new List<Game>();
            Task.Delay(100);
            json = DataAccess.FindGamesForPlayer(User.Email);
            if ((bool)json.SelectToken("success"))
            {
                JArray data = (json.SelectToken("data") as JArray);
                foreach (var val in data)
                {
                    int gameId = (int) (((JObject)val[0]).SelectToken("gameId"));
                    games.Add(new Game() { GameId = gameId, Name= "Game ID = " + gameId, IsUserJoined=true});
                }
            }

            this.lvGames.ItemsSource = games;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(CreateGame));
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(FindGame));
        }

        private void btnMap_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ViewMap));
        }

        private void lvGames_ItemClick(object sender, ItemClickEventArgs e)
        {
            Game game = e.ClickedItem as Game;
            Frame.Navigate(typeof(GameDetails), game);
        }
    }
}