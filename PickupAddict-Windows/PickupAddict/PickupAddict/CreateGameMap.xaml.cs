﻿using Newtonsoft.Json.Linq;
using PickupAddict.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace PickupAddict
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateGameMap : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private MapIcon tempMapItem;
        private Game game;

        public CreateGameMap()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {

        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Retrieve and add location to map in background
            await Task.Run(() => RetrieveLocationsAndAddToMap());

            try
            {
                game = e.Parameter as Game;


                // Getting Current Location  
                var geoLocator = new Geolocator();
                geoLocator.DesiredAccuracyInMeters = 10;
                Geoposition geoposition = await geoLocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10));


                // Create a circle
                Ellipse fence = new Ellipse();
                fence.Width = 20;
                fence.Height = 20;
                fence.Stroke = new SolidColorBrush(Colors.Blue);
                fence.StrokeThickness = 10;
                fence.Opacity = .75;
                MapControl.SetLocation(fence, geoposition.Coordinate.Point);
                MapControl.SetNormalizedAnchorPoint(fence, new Point(0.5, 0.5));
                MapControl.Children.Add(fence);
                await MapControl.TrySetViewAsync(geoposition.Coordinate.Point, 12D, 0, 0, MapAnimationKind.Bow);

            }
            catch (UnauthorizedAccessException)
            {
                var md = new MessageDialog("Location service is turned off!");
                var x = md.ShowAsync();
            }
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }



        #endregion


        private void RetrieveLocationsAndAddToMap()
        {
            var result = DataAccess.GetAllLocations();
            if ((bool)result.SelectToken("success"))
            {
                var data = result.SelectToken("data") as JArray;
                foreach (var obj in data)
                {
                    Location loc = new Location(obj[0] as JObject);
                    AddMapIcon(loc);
                }
            }
        }


        private void AddMapIcon(Location loc)
        {
            MapControl.MapElements.Add(new MapIcon()
            {
                Location = new Geopoint(loc.Geoposition),
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                Title = loc.Description
            });
        }

        private async void btnUseLocation_Click(object sender, RoutedEventArgs e)
        {
            JObject result = null;
            var pos = tempMapItem.Location.Position;
            await Task.Run(() =>
            {
                result = DataAccess.CreateLocationAndGame(game, pos.Latitude, pos.Longitude, "No description available"/*TODO : description*/);
            });

            if ((bool)result.SelectToken("success"))
            {
                var data = result.SelectToken("data") as JObject;
                result = DataAccess.AddPlayerToGame(new Game() { GameId = (int)data.SelectToken("gameId")}, 
                                                        DataAccess.LoadFromApplicationDataContainer("email"));
                if ((bool)result.SelectToken("success"))
                    Frame.Navigate(typeof(MainPage));
                else
                    await new MessageDialog((string)result.SelectToken("errormsg")).ShowAsync();
            }
            else
            {
                await new MessageDialog((string)result.SelectToken("errormsg")).ShowAsync();
            }
        }

        private async void MapControl_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            if (tempMapItem != null)
            {
                MapControl.MapElements.Remove(tempMapItem);
            }
            else
            {
                lblInstructions.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                btnUseLocation.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            tempMapItem = new MapIcon()
            {
                Location = args.Location,
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                Title = "New Location"
            };

            MapControl.MapElements.Add(tempMapItem);
            await MapControl.TrySetViewAsync(tempMapItem.Location, 18D, 0, 0, MapAnimationKind.Linear);
        }
    }
}