﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickupAddict
{
    public class Game
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Location Location { get; set; }
        public Sport Sport { get; set; }
        public int MinPlayers { get; set; }
        public int MaxPlayers { get; set; }
        public int CurrentPlayers { get; set; }
        public bool IsUserJoined { get; set; }

        public string DateAndTimeDescription
        {
            get
            {
                return Start.ToString("MMM dd") + " from " + Start.ToString("HH:mm") + " to " + End.ToString("HH:mm a");
            }
        }

        public Game()
        {

        }
    }
}
