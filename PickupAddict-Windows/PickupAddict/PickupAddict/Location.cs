﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace PickupAddict
{
    public class Location
    {
        public int LocationId { get; set; }
        public double Latitude {get; set;}
        public double Longitude { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public BasicGeoposition Geoposition
        {
            get
            {
                var geo = new BasicGeoposition();
                geo.Latitude = this.Latitude;
                geo.Longitude = this.Longitude;
                return geo;
            }
        }

        public Location()
        {

        }

        public Location(JObject json)
        {
            this.LocationId = (int)json.SelectToken("locationId");
            this.Latitude = (double)json.SelectToken("latitude");
            this.Longitude = (double)json.SelectToken("longitude");
            this.Description = (string)json.SelectToken("description");
            this.Address = (string)json.SelectToken("houseNumber")
                         + (string)json.SelectToken("address")
                         + (string)json.SelectToken("city")
                         + (string)json.SelectToken("state")
                         + (string)json.SelectToken("zipcode");
        }
    }
}
