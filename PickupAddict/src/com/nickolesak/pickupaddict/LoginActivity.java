package com.nickolesak.pickupaddict;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


public class LoginActivity extends Activity {

	/**
	 * On Create
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        ActionBar actionBar = getActionBar();
        actionBar.hide();
        initComponents();
	}
	
	private void initComponents() {
		Button btnLogin = (Button) this.findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (isLoginInformationValid()) {
					Intent i = new Intent(v.getContext(), MainActivity.class);
					startActivity(i);
				}
			}
		});
	}
	
	private boolean isLoginInformationValid() {
		
		new ValidateCredentials().execute();
		return true;
	}
	

	private class ValidateCredentials extends AsyncTask <Void, Void, String> 
	{
		protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException 
		{
			InputStream in = entity.getContent();
	
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n>0) 
			{
				byte[] b = new byte[4096];
				n =  in.read(b);
				if (n>0)
					out.append(new String(b, 0, n));
			}
	
			return out.toString();
		}
		
		public String doInBackground(Void... params) 
		{
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			//EditText user = (EditText) findViewById((R.id.)
			HttpGet httpGet = new HttpGet("http://www.pickupaddict.com/method_name.php?variable=1");
			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				text = getASCIIContentFromEntity(entity);
			} catch (Exception e) {
				return e.getLocalizedMessage();
			}

			return text;
		}
		
		protected void onPostExecute(String results) 
		{
			Intent intent = new Intent(null, MainActivity.class);
			//StartActivity(intent);
		}
	}
	

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.actions, menu);
        return true;
    }

	
}
