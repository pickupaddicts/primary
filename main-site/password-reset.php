<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Reset your Password</h1>
					<hr />
					<br />
					<div class="form">
						<form action="form-files/password-reset.php" enctype="multipart/form-data" method="post" onsubmit="return checkForm(this);">
							<p>Please enter your email address to reset your password:</p>
							<input type="email" name="username" id="username" placeholder="Email Address" required="true">
							<br />
							<button type="submit" style="margin-top: 70px;">Send Password Reset</button>
						</form>
					</div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>