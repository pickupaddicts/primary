<?php
	include 'include/header.php';
?>

<html>
	<head>
		<title></title>
		<?php include 'include/head.php'; ?>
	</head>
	<body>
		<div class="navigation">
			<?php include 'include/navigation.php'; ?>
		</div>
		<div class="banner">
			<img src="images/pickupaddict.png" style="height: 100px; margin-top: 65px;" />
		</div>
		<div id="container">
			<h3>Hello <?php echo $_SESSION['firstName'] . " " . $_SESSION['lastName']; ?>...</h3>
			<br />
			<h4>Games you've joined</h4>
			<hr />
			<?php
				$gameQuery = "SELECT 
					g.publicId,
					g.title,
					g.description,
					g.startTime,
					g.endTime,
					l.latitude,
					l.longitude,
					l.houseNumber,
					l.address,
					l.city,
					l.state,
					l.zipcode
				FROM
					game_table g,
					game_user_table gu,
					location_table l
				WHERE
					g.gameId = gu.gameId AND
					gu.userId = '" . $userId . "' AND
					g.locationId = l.locationId
				";

				$result = mysqli_query($con, $gameQuery);

				while($row = mysqli_fetch_array($result)) {
					$start = date('F j, Y g:i A', strtotime($row['startTime']));
            		$end = date('F j, Y g:i A', strtotime($row['endTime']));
					echo "</p><b>Game Title:</b> " . $row['title'] . "<br />";
					echo "<b>Game Description:</b> " . $row['description'] . "<br />";
					echo "<b>Game Start:</b> " . $start . "<br />";
					echo "<b>Game End:</b> " . $end . "<br />";
					echo "<b>Game Location:</b> " . $row['houseNumber'] . " " . $row['address'] . ", " . $row['city'] . ", " . $row['state'] . " " . $row['zipcode'] . "<br />";
					echo "<b>Game Coordinates:</b> (" . $row['latitude'] . ", " . $row['longitude'] . ")</p>";
					//echo "<a href='remove-game.php?gameId=" . $row['publicId'] . "' class='btn btn-blue'>Remove</a><br /><br />";
			    }
			?>
		</div>
	</body>
</html>