<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/navigation.css">
<link rel="stylesheet" type="text/css" href="css/layout.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/DateTimePicker-ltie9.css">
<link rel="stylesheet" type="text/css" href="css/DateTimePicker.css">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLXSJOqhai-Pr8YtnUiIpLmLiqsoPN0NU"></script>
<script type='text/javascript' src='javascript/geolocation.js'></script>
<script type='text/javascript' src='javascript/geolocation-create.js'></script>
<script src="javascript/form_checker.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="javascript/DateTimePicker-ltie9.js"></script>
<script src="javascript/DateTimePicker.js"></script>
