<?php
	include 'include/header.php';
?>

<html>
	<head>
		<title></title>
		<?php include 'include/head.php'; ?>
		<script>
		  	$(function() {
				$( "#datepicker" ).datepicker();
				$('.dtPicker').DateTimePicker({
					isPopup: false,
                    timeFormat: "hh:mm AA" 
				});  
			});
		</script>
	</head>
	<body onload="initializeCreate();">
		<div class="navigation">
			<?php include 'include/navigation.php'; ?>
		</div>
		<div id="container">
			<h1>Create a Game</h1>
			<hr />
			<br />
			<form action="form-files/game-post.php" enctype="multipart/form-data" method="post" onsubmit="return checkForm(this);">
				<input type="hidden" id="gameId" name="gameId" value="<?php echo '' . md5(uniqid(rand(), true)); ?>">
				<input type="hidden" id="locationId" name="locationId" value="<?php echo '' . md5(uniqid(rand(), true)); ?>">
				<input type="hidden" id="latitude" name="latitude" value="">
				<input type="hidden" id="longitude" name="longitude" value="">
				<input type="hidden" id="houseNumber" name="houseNumber" value="">
				<input type="hidden" id="address" name="address" value="">
				<input type="hidden" id="city" name="city" value="">
				<input type="hidden" id="state" name="state" value="">
				<input type="hidden" id="zip" name="zip" value="">
				<fieldset>
					<select id='sports' name='sports' style="width: 250px;" required="true">
						<option value="">Please select a sport</option>
						<?php
							$query = "SELECT publicKey, name FROM sports_table WHERE deleteDate IS NULL";
							$result = mysqli_query($con, $query);

							while($row = mysqli_fetch_array($result)) {
								echo "<option value='" . $row['publicKey'] . "'>" . $row['name'] . "</option>";
					        }
						?>
					</select>
					<input type="text" id="gameTitle" name="gameTitle" style="width: 350px; margin-left: 20px;" placeholder="Please enter a game title" required="true">
				</fieldset>
				<br />
				<h4>Select a game location</h4>
				<input type="text" id="zipcode" name="zipcode" style="width: 250px; border: 1px solid #292929;" placeholder="Search by location">
				<button type="button" onclick="codeAddressCreate('zipcode');" class="btn btn-blue" style="width: 125px;">Search</button>
				<button type="button" onclick="setLocation();" id="useLocation" class="btn btn-green" style="width: 250px; display: none;">Use Location</button>
				<div style="width: 100%; height: 400px;" id="map-canvas">
				
				</div>
				<br />
				<br />
				<fieldset style="text-align: center;">
					<input type="text" id="datepicker" name="gameDate" style="width: 250px; margin-right: 20px;" placeholder="Select a game date" required="true">
					<input type="text" class="timepicker" name="startTime" data-field="time" style="width: 250px; display: inline-block; margin-right: 20px;" placeholder="Select a start time" required="true" readonly>
					<input type="text" class="timepicker" name="endTime" data-field="time" style="width: 250px; display: inline-block; margin-right: 20px;" placeholder="Select a end time" required="true" readonly>
					<input type="number" id="minPlayers" name="minPlayers" style="width: 250px; margin-right: 20px;" placeholder="Minimum players" required="true">
					<input type="number" id="maxPlayers" name="maxPlayers" style="width: 250px;" placeholder="Maximum players" required="true">
					<div class="dtPicker"></div>
					<div class="dtPicker"></div>
				</fieldset>
				<textarea id="description" name="description" placeholder="Please enter a description for the new game..."></textarea>
				<button type="submit" style="margin-top: 50px;">Create Game</button>
			</form>
		</div>
	</body>
</html>