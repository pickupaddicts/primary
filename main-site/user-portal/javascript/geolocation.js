var geocoder;
var map;

function initialize() {
	geocoder = new google.maps.Geocoder();
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else { 
		var mapOptions = {
			center: { lat: 38.6880839, lng: -98.7819212},
			zoom: 4
		};
		mapOptions.scrollwheel = false;
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}
}

function showPosition(position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	var mapOptions = new Object();
	var centerCoords = new Object();
	centerCoords.lat = latitude;
	centerCoords.lng = longitude;
	mapOptions.center = centerCoords;
	mapOptions.zoom = 14;
	mapOptions.scrollwheel = false;
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function() {
	  	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	    	eval(xmlhttp.responseText);
	    }
	}
	var url = "support/get-games.php?latitude=" + latitude + "&longitude=" + longitude;
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function codeAddress(input) {
    var address = document.getElementById(input).value;
    geocoder.geocode( { 'address': address}, function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
        	map.setCenter(results[0].geometry.location);
        	map.setZoom(14);

        	var xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function() {
			  	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			    	eval(xmlhttp.responseText);
			    }
			}
			var url = "support/get-games.php?latitude=" + results[0].geometry.location.lat() + "&longitude=" + results[0].geometry.location.lng();
			xmlhttp.open("GET", url, true);
			xmlhttp.send();
      	} else {
        	alert("Geocode was not successful for the following reason: " + status);
      	}
    });
}

function addMarker(latitude, longitude, description) {
	var coords = new google.maps.LatLng(latitude, longitude);
	var marker = new google.maps.Marker({
	    position: coords,
	    map: map,
	    title: description
	});
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
        	var mapOptions = {
				center: { lat: 38.6880839, lng: -98.7819212},
				zoom: 4
			};
			mapOptions.scrollwheel = false;
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            break;
        case error.POSITION_UNAVAILABLE:
            var mapOptions = {
				center: { lat: 38.6880839, lng: -98.7819212},
				zoom: 4
			};
			mapOptions.scrollwheel = false;
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            break;
        case error.TIMEOUT:
            var mapOptions = {
				center: { lat: 38.6880839, lng: -98.7819212},
				zoom: 4
			};
			mapOptions.scrollwheel = false;
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            break;
        case error.UNKNOWN_ERROR:
            var mapOptions = {
				center: { lat: 38.6880839, lng: -98.7819212},
				zoom: 4
			};
			mapOptions.scrollwheel = false;
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            break;
    }
}