var geocoder;
var map;
var marker;
var markerCheck = 0;

function initializeCreate() {
	geocoder = new google.maps.Geocoder();
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPositionCreate, showError);
	} else { 
		var mapOptions = {
			center: { lat: 38.6880839, lng: -98.7819212},
			zoom: 4
		};
		mapOptions.scrollwheel = false;
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}
}

function showPositionCreate(position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	var mapOptions = new Object();
	var centerCoords = new Object();
	centerCoords.lat = latitude;
	centerCoords.lng = longitude;
	mapOptions.center = centerCoords;
	mapOptions.zoom = 14;
	mapOptions.scrollwheel = false;
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	google.maps.event.addListener(map, 'click', function(event) {
    	addMarkerCreate(event.latLng.lat(), event.latLng.lng(), "");
  	});
}

function codeAddressCreate(input) {
    var address = document.getElementById(input).value;
    geocoder.geocode( { 'address': address}, function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
        	map.setCenter(results[0].geometry.location);
        	map.setZoom(14);
      	} else {
        	alert("Geocode was not successful for the following reason: " + status);
      	}
    });
}

function addMarkerCreate(latitude, longitude, description) {
	if(markerCheck == 1) {
		marker.setMap(null);
	}
	markerCheck = 1;
	var coords = new google.maps.LatLng(latitude, longitude);
	marker = new google.maps.Marker({
	    position: coords,
	    map: map,
	    animation: google.maps.Animation.BOUNCE,
	    title: description
	});
	document.getElementById('useLocation').style.display = 'inline-block';
}

function setLocation() {
	document.getElementById('latitude').value = marker.position.lat();
	document.getElementById('longitude').value = marker.position.lng();
	geocoder.geocode({'latLng': marker.position}, function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
			document.getElementById('houseNumber').value = results[0].address_components[0].short_name;
			document.getElementById('address').value = results[0].address_components[1].long_name;
			document.getElementById('city').value = results[0].address_components[2].short_name;
			document.getElementById('state').value = results[0].address_components[5].short_name;
			document.getElementById('zip').value = results[0].address_components[7].long_name;
      	} else {
        	alert("Geocode was not successful for the following reason: " + status);
      	}
    });
	alert("Your location has been set.");
	marker.setAnimation(null);
}