<?php
	include 'include/header.php';
?>

<html>
	<head>
		<title></title>
		<?php include 'include/head.php'; ?>
	</head>
	<body onload="initialize();">
		<div class="navigation">
			<?php include 'include/navigation.php'; ?>
		</div>
		<div id="container">
			<h1>Find a Game</h1>
			<hr />
			<br />
			<input type="text" id="zipcode" name="zipcode" style="width: 250px; border: 1px solid #292929;" placeholder="Search by location">
			<button type="button" onclick="codeAddress('zipcode');" class="btn btn-blue" style="width: 125px;">Search</button>
			<a href="create-game.php" class="btn btn-red">Create New Game</a>
			</form>
			<div style="width: 100%; height: 400px;" id="map-canvas">
				
			</div>
			<br />
			<div class="results" id="results">
				<h3>Games near you...</h3>
				<hr />
			</div>
		</div>
	</body>
</html>