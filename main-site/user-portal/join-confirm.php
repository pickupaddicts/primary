<?php
	include 'include/header.php';
?>

<html>
	<head>
		<title></title>
		<?php include 'include/head.php'; ?>
	</head>
	<body>
		<div class="navigation">
			<?php include 'include/navigation.php'; ?>
		</div>
		<div id="container">
			<div style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
				<h1>Join a Game</h1>
				<hr />
				<br />
				<p style="text-align: center;">
					You were successfully added to the game.
					<br />
					<a href="index.php" class="btn btn-blue" style="margin-top: 50px;">Return to Dashboard</a>
				</p>
			</div>
		</div>
	</body>
</html>