<?php
	session_name('pickupaddict');
	session_start();

	if($_SERVER[HTTP_HOST] == "localhost") {
		$server = "http://localhost/primary/main-site/";
	}
	else {
		$server = "http://pickupaddict.com/";
	}

	$host = "50.87.148.155";
	$username = "crivers_addict";
	$password = "Pickup2014";
	$database = "crivers_pickup_addict";

	$con = mysqli_connect($host, $username, $password, $database);

	// Check connection
	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	$publicId = $_SESSION['publicId'];
	$publicId = trim($publicId);
	$username = $_SESSION['email'];
	$username = trim($username);

	$query = "SELECT 1 FROM user_table WHERE publicId = '" . $publicId . "' AND email = '" . $username . "'";
	$result = mysqli_query($con, $query);
	
    if(mysqli_num_rows($result) != 1) {
    	header('Location: ' . $server . 'error.php?error=5');
    	mysqli_close($con);
    	exit;
    }

    $selectQuery = "SELECT 1 FROM game_table WHERE publicId = '" . $_POST['publicId'] . "'";
    $result = mysqli_query($con, $selectQuery);

	if(mysqli_num_rows($result) == 0) {
		$insertLocation = "INSERT INTO location_table (
			publicId, 
			latitude, 
			longitude,
			houseNumber,
			address,
			city,
			state,
			zipcode,
			description,
			entryDate
		) 
		VALUES (
			'" . $_POST['locationId'] . "',
			'" . $_POST['latitude'] . "',
			'" . $_POST['longitude'] . "',
			'" . $_POST['houseNumber'] . "',
			'" . $_POST['address'] . "',
			'" . $_POST['city'] . "',
			'" . $_POST['state'] . "',
			'" . $_POST['zip'] . "',
			'No description available',
			NOW()
		)";
		$resultLocation = mysqli_query($con, $insertLocation);

    	$startDate = $_POST['gameDate'] . " " . $_POST['startTime'];
    	$startDate = date_create($startDate);
		$startDate = date_format($startDate, 'Y-m-d H:i:s');
    	$endDate = $_POST['gameDate'] . " " . $_POST['endTime'];
    	$endDate = date_create($endDate);
		$endDate = date_format($endDate, 'Y-m-d H:i:s');

		$description = $_POST['description'];
		if($description == "") {
			$description = "No description available";
		}

    	$insertGame = "INSERT INTO game_table (
    		publicId,
    		locationId,
    		sportId,
    		title,
    		description,
    		startTime,
    		endTime,
    		minPlayers,
    		maxPlayers,
    		entryDate
    	)
		VALUES (
			'" . $_POST['gameId'] . "',
			(SELECT locationId FROM location_table WHERE publicId = '" . $_POST['locationId'] . "'),
			(SELECT sportId FROM sports_table WHERE publicKey = '" . $_POST['sports'] . "'),
			'" . $_POST['gameTitle'] . "',
			'" . $description . "',
			'" . $startDate . "',
			'" . $endDate . "',
			'" . $_POST['minPlayers'] . "',
			'" . $_POST['maxPlayers'] . "',
			NOW()
    	)";

		$resultGame = mysqli_query($con, $insertGame);

		header('Location: ../game-confirm.php');
    }    
?>