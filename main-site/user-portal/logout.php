<?php
	if($_SERVER[HTTP_HOST] == "localhost") {
		$server = "http://localhost/primary/main-site/";
	}
	else {
		$server = "http://pickupaddict.com/";
	}

	session_name('pickupaddict');
	session_start();
	$_SESSION['publicId'] = "";
	$_SESSION['email'] = "";
	$_SESSION['firstName'] = "";
   	$_SESSION['lastName'] = "";

   	header('Location: ' . $server . 'logout.php');
?>