<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Log in to your account</h1>
					<hr />
					<br />
					<div class="form">
						<form action="form-files/login-post.php" enctype="multipart/form-data" method="post" onsubmit="return checkForm(this);">
							<input type="email" name="username" id="username" placeholder="Email Address" required="true">
							<br />
							<input type="password" name="password" id="password" placeholder="Password" required="true">
							<button type="submit" style="margin-top: 70px;">Login</button>
							<a href="password-reset.php" style="font-size: 16px; text-align: center; display: block; margin-top: 10px;">Forgot your password?</a>
						</form>
					</div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>