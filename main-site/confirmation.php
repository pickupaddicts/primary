<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Confirm Your Email Account</h1>
					<hr />
					<br />
					<p style="min-height: 400px;">Please go to your email and confirm that the email entered is actually your email account. Your account will not be active until your account is confirmed.</p>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>