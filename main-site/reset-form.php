<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Reset Your Account Password</h1>
					<hr />
					<br />
					<div class="form">
						<form action="form-files/password-post.php" enctype="multipart/form-data" method="post" onsubmit="return checkForm(this);">
							<p>Please enter your new password.</p>
							<input type="hidden" name="publicId" id="publicId" value="<?php echo '' . $_GET['publicId']; ?>">
							<input type="password" name="password" id="password" placeholder="Password" required="true">
							<button type="submit" style="margin-top: 70px;">Reset Password</button>
						</form>
					</div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>