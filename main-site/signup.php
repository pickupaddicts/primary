<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Sign up for your account</h1>
					<hr />
					<br />
					<div class="form">
						<form action="form-files/signup-post.php" enctype="multipart/form-data" method="post" onsubmit="return checkForm(this);">
							<input type="hidden" name="publicId" id="publicId" value="<?php echo '' . md5(uniqid(rand(), true)); ?>">
							<input type="text" name="firstName" id="firstName" placeholder="First Name" required="true">
							<input type="text" name="lastName" id="lastName" placeholder="Last Name" required="true">
							<br />
							<br />
							<input type="email" name="username" id="username" placeholder="Email Address" required="true">
							<input type="password" name="password" id="password" placeholder="Password" required="true">
							<button type="submit" style="margin-top: 70px;">Sign Up!</button>
						</form>
					</div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>