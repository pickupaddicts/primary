<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div  style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>Password Reset Request Processed</h1>
					<hr />
					<br />
					<p style="min-height: 400px;">Your password reset request has been processed. Please check your email for further instructions.</p>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>