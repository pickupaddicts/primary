<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
		<script type="text/javascript">
			var fixed = false;
			document.onscroll = function() {
				if(document.body.scrollTop >= 250) {
					if(!fixed) {
						fixed = true;
						var nav = document.getElementById("navigation").style;
						nav.position = "fixed";
						nav.top = "0px";
						document.getElementById("content").setAttribute("style", "margin-top: 135px");
					}
				}
				else {
					if(fixed) {
						fixed = false;
						var nav = document.getElementById("navigation").style;
						nav.position = "relative";
						nav.top = "";
						document.getElementById("content").setAttribute("style", "margin-top: 0px");
					}
				}
			}
		</script>
	</head>
	<body>
		<div id="container">
			<div class="banner">
				<img src="images/pickupaddict.png" style="height: 100px; margin-top: 65px;" />
			</div>
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<h1>Where's your next game?</h1>
				<div style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<div style="float: left;">
						<img src="images/iphone.png" style="height: 500px;" />
					</div>
					<div style="float: right; width: 550px;">
						<iframe width="550px" height="500px" src="//www.youtube.com/embed/Nh36RITv30o" frameborder="0" allowfullscreen></iframe>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>