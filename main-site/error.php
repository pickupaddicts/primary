<?php include 'include/header.php'; ?>
<html>
	<head>
		<title>Pickup Addicts: Submission Site</title>
		<?php include 'include/links.php'; ?>
	</head>
	<body>
		<div id="container">
			<?php include 'include/navigation.php'; ?>
			<div id="content" class="content-container">
				<div style="width: 960px; display: block; margin-top: 75px; margin-bottom: 125px; margin-left: auto; margin-right: auto;">
					<h1>There was an Error!</h1>
					<hr />
					<br />
					<?php
						if(isset($_GET['error'])) {
							include 'errors/' . $_GET['error'] . '.html';
						}
						else {
							echo "<p style='min-height: 400px;'>An unknown error occured. If the problem persists, please contact technical support to resolve the issue.</p>";
						}
					?>
				</div>
			</div>
			<div class="footer">

			</div>
		</div>
	</body>
</html>