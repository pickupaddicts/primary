<?php
class UserItem {
    public $session_id;
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $publicId;
    public $deleteDate;
    public $entryDate;
    public $birthday;
    public $gender;
    public $phoneNumber;
    public $address1;
    public $address2;
    public $city;
    public $state;
    public $country;
    public $zipCode;

    public $host = '50.87.148.155';
    public $username = 'crivers_addict';
    public $passphrase = 'Pickup2014';
    public $database = 'crivers_pickup_addict';
    
    public function create($email, $password) {
        //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        //get the username/password hash
        $userhash = md5("{$email}_{$password}");
        $password = md5(trim("{$password}"));

        //if the $session_id isn't set yet, it means we need to create a new session item
        if(is_null($this->session_id) || !is_numeric($this->session_id) ) {
            //the todo id is the current time
            $this->session_id = time();
        }
         
        $publicId = md5("{$email}");
        $firstName = $this->firstName;
        $lastName = $this->lastName;
        date_default_timezone_set(UTC);
        $entryDate =  date("d-m-y : H:i:s", time());
        $birthday = $this->birthday;
        $gender = $this->gender;
        $phoneNumber = $this->phoneNumber;
        $address1 = $this->address1;
        $address2 = $this->address2;
        $city = $this->city;
        $state = $this->state;
        $country = $this->country;
        $zipCode = $this->zipCode;

        //create the query to create a new row for the new user
        $query = "INSERT INTO user_table (apiKey, email, password, firstName, lastName, publicId, deleteDate, entryDate, birthday, gender, phoneNumber, address1, address2, city, state, country, zipCode) VALUES
        ('$userhash', '$email', '$password', '$firstName', '$lastName', '$publicId', '$deleteDate', '$entryDate', '$birthday', '$gender', '$phoneNumber', '$address1', '$address2', '$city', '$state', '$country', '$zipCode')";

        //save the serialized array version into a file
        $success = mysqli_query($con, $query);
         
        //if saving was not successful, throw an exception
        if($success == false ) {
            throw new Exception('Failed to save user info');
        }

        $query = "SELECT apiKey FROM user_table WHERE publicId = '$publicId'";
        $result = mysqli_query($con, $query);
        $value = mysqli_fetch_object($result);
        $userId = $value->userId;

         //print($array);

         //close the database
         mysqli_close($con);

        //return the array version
        return array(
            'session_id' => $this->session_id,
            'email' => $this->email,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName);
    }
    
    public function login($email, $password)
    {
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
            //get the username/password hash
            $userhash = md5("{$email}_{$password}");
            $password = md5(trim("{$password}"));
            $date = "0000-00-00 00:00:00";

            //use the userhash as the unique identifier to get a user's row of information
            $query = "SELECT * FROM user_table  WHERE apiKey = '$userhash'";
            $result = mysqli_query($con, $query);

            //fetches the appropriate row from the database
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                //on a false return from row, throw a user doesn't exist exception
                throw new Exception('Error - user does not exist');
            }
            if(!($row["deleteDate"] == $date)){
                throw new Exception('Error - user has been deleted');
            }

            //close the database
            mysqli_close($con);

            //return the user's info as an array
            return $row;
    }

    public function confirm()
    {
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
            //get the username/password hash
            $userId = $this->userId;

            //use the userhash as the unique identifier to get a user's row of information
            $query = "UPDATE user_table SET confirmed=1 WHERE userId = '$userId'";
            $result = mysqli_query($con, $query);

            if(mysqli_affected_rows($con) > -1) {
                $data = "User successfully confirmed.";
            }
            else{
                $data = "User does not exist";
            }

            mysqli_close($con);

            return $data;
    }

    public function check($email, $password)
    {
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
            //get the username/password hash
            $userhash = md5("{$email}_{$password}");
            $password = md5(trim("{$password}"));

            //use the userhash as the unique identifier to get a user's row of information
            $query = "SELECT confirmed FROM user_table  WHERE apiKey = '$userhash'";
            $result = mysqli_fetch_assoc(mysqli_query($con, $query));

            //fetches the appropriate row from the database
            if (empty($result))
            {
                //on a false return from row, throw a user doesn't exist exception
                throw new Exception('Error - user does not exist');
            }  

            //close the database
            mysqli_close($con);

            //return the user's info as an array
            return $result['confirmed'];
    }

    public function update($email, $password)
    {
            /*$mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            //get the username/password hash
            $userhash = md5("{$email}_{$password}");
            $password = md5(trim("{$password}"));
            $publicId = md5("{$email}");

            $query = "UPDATE user_table SET "*/
    }

    public function delete($email, $password)
    {
            $mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            //get the username/password hash
            $userhash = md5("{$email}_{$password}");
            date_default_timezone_set(UTC);
            $date = date("d-m-y : H:i:s", time());

            $query = "UPDATE user_table SET deleteDate = '$date' WHERE apiKey = '$userhash'";

            $result = mysqli_query($con, $query);

            mysqli_close($con);

            return $result;
    }

    public function show(){
                      //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $query = "SELECT * FROM user_table";

        $result = mysqli_query($con, $query);

        $i = 0;
        while($row = $result->fetch_assoc()){
            $users[$i] = array($row);
            $i++;
        }

        mysqli_close($con);

        return $users;
    }

    public function del(){
            $mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            $query = "DELETE FROM user_table";

            $result = mysqli_query($con, $query);

            mysqli_close($con);

            return $result;
    }

    public function toArray()
    {
        //return an array version of the todo item
        return array(
            'session_id' => $this->session_id,
            'email' => $this->email,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName);
    }
}