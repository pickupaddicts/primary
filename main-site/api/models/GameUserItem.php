 <?php
class GameUserItem {
    public $gameId;
    public $userId;
    public $entryDate;
    public $deleteDate;

    public $host = '50.87.148.155';
    public $username = 'crivers_addict';
    public $passphrase = 'Pickup2014';
    public $database = 'crivers_pickup_addict';
    
    public function create() {
        //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
         
        $gameId = $this->gameId;
        $userId = $this->userId;
        date_default_timezone_set(UTC);
        $entryDate =  date("d-m-y : H:i:s", time());;
        $publicId = md5("{$gameId}_{$userId}");
        $deleteDate = "0000-00-00 00:00:00";

        //create the query to create a new row for the new user
        $query = "INSERT INTO game_user_table (publicId, gameId, userId, entryDate, deleteDate) VALUES
        ('$publicId', '$gameId', '$userId', '$entryDate', '$deleteDate')";

        //save the serialized array version into a file
        $success = mysqli_query($con, $query);
         
        //if saving was not successful, throw an exception
        if( $success == false ) {
            throw new Exception('Failed to add user to game.');
        }
         
         $array = $this->toArray();

         //close the database
         mysqli_close($con);

        //return the array version
        return $array;
    }

    public function find()
    {
               //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }


        $gameId = $this->gameId;
        $userId = $this->userId;

        if(!empty($gameId) && !empty($userId)){
             $query = "SELECT * FROM game_user_table WHERE gameId = '$gameId' AND userID = '$userId'";
        }
        else if(empty($gameId) && !empty($userId)){
             $query = "SELECT * FROM game_user_table WHERE userId = '$userId'";
        }
        else if(!empty($gameId) && empty($userId)){
             $query = "SELECT * FROM game_user_table WHERE gameId = '$gameId'";
        }
        else{
             $query = "SELECT * FROM game_user_table";
        }

        $result = mysqli_query($con, $query);

        $i = 0;
        while($row = $result->fetch_assoc()){
            $gameUsers[$i] = array($row);
            $i++;
        }

        mysqli_close($con);

        return $gameUsers;
    }

    public function delete()
    {
            $mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            $gameId = $this->gameId;
            $userId = $this->userId;

            date_default_timezone_set(UTC);
            $date = date("d-m-y : H:i:s", time());

            $query = "DELETE FROM game_user_table WHERE gameId='$gameId' AND userId='$userId'";

            $result = mysqli_query($con, $query);

            mysqli_close($con);

            return $result;
    }

        public function del(){
            $mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            $query = "DELETE FROM game_user_table";

            $result = mysqli_query($con, $query);

            mysqli_close($con);

            return $result;
    }

    public function toArray()
    {
        //return an array version of the todo item
        return array(
            'gameId' => $this->gameId,
            'userId' => $this->userId,
            'entryDate' => $this->entryDate,
            'deleteDate' => $this->deleteDate
        );
    }
}