<?php
class LocationItem {
    public $locationId;
    public $publicId;
    public $latitude;
    public $longitude;
    public $houseNumber;
    public $address;
    public $city;
    public $state;
    public $zipcode;
    public $description;

    public $host = '50.87.148.155';
    public $username = 'crivers_addict';
    public $passphrase = 'Pickup2014';
    public $database = 'crivers_pickup_addict';
    
    public function createCoor() {
        //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $houseNumber = $this->houseNumber;
        $address = $this->address;
        $city = $this->city;
        $state = $this->state;
        $zipcode = $this->zipcode;
        $string = $houseNumber . " " . $address . " " . $city . " " . $state . " " . $zipcode;
        $fullAddress = str_replace (" ", "+", urlencode($string));

        $request = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$fullAddress."&key=AIzaSyBGqaU-Na4QIzsdy4z-E5XM9huf7_ufgyE");
        $json = json_decode($request, true);

        $latitude = $json['results'][0]['geometry']['location']['lat'];
        $longitude = $json['results'][0]['geometry']['location']['lng'];
        $description = $this->description;
        $publicId = md5("{$latitude}_{$longitude}");

        //create the query to create a new row for the new user
        $query = "INSERT INTO location_table (publicId, latitude, longitude, houseNumber, address, city, state, zipcode, description) VALUES
        ('$publicId', '$latitude', '$longitude', '$houseNumber', '$address', '$city', '$state', '$zipcode', '$description')";

        //save the serialized array version into a file
        $success = mysqli_query($con, $query);
         
        //if saving was not successful, throw an exception
        if( $success == false ) {
            throw new Exception('Failed to save location info');
        }

        $query = "SELECT locationId FROM location_table WHERE latitude = '$latitude' AND longitude = '$longitude'";
        $result = mysqli_query($con, $query);
        $value = mysqli_fetch_object($result);
        $locationId = $value->locationId;
         
         $array = array('locationId' => $locationId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'houseNumber' => $houseNumber,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zipcode' =>$zipcode,
            'description' => $description);

         //close the database
         mysqli_close($con);

        //return the array version
        return $array;
    }
    
        public function createAddr() {
        //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $latitude = $this->latitude;
        $longitude = $this->longitude;
        
        $request = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=".$latitude.",".$longitude."&key=AIzaSyBGqaU-Na4QIzsdy4z-E5XM9huf7_ufgyE");
        $json = json_decode($request, true);

        $houseNumber = $json['results'][0]['address_components'][0]['short_name'];
        $address = $json['results'][0]['address_components'][2]['long_name'];
        $city = $json['results'][0]['address_components'][3]['short_name'];
        $state = $json['results'][0]['address_components'][6]['short_name'];
        $zipcode = $json['results'][0]['address_components'][8]['short_name'];
        $description = $this->description;
        $publicId = md5("{$latitude}_{$longitude}");

        //create the query to create a new row for the new user
        $query = "INSERT INTO location_table (publicId, latitude, longitude, houseNumber, address, city, state, zipcode, description) VALUES
        ('$publicId', '$latitude', '$longitude', '$houseNumber', '$address', '$city', '$state', '$zipcode', '$description')";

        //save the serialized array version into a file
        $success = mysqli_query($con, $query);
         
        //if saving was not successful, throw an exception
        if( $success == false ) {
            throw new Exception('Failed to save location info');
        }

        $query = "SELECT locationId FROM location_table WHERE latitude = '$latitude' AND longitude = '$longitude'";
        $result = mysqli_query($con, $query);
        $value = mysqli_fetch_object($result);
        $locationId = $value->locationId;
         
         $array = array('locationId' => $locationId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'houseNumber' => $houseNumber,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zipcode' =>$zipcode,
            'description' => $description);

         //close the database
         mysqli_close($con);

        //return the array version
        return $array;
    }

    public function find()
    {
     //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $locationId = $this->locationId;
        $latitude= $this->latitude;
        $longitude = $this->longitude;
        $houseNumber = $this->houseNumber;
        $address = $this->address;
        $city = $this->city;
        $state = $this->state;
        $zipcode = $this->zipcode;

        if(!empty($latitude)){
            $query = "SELECT * FROM location_table  WHERE latitude = '$latitude' AND longitude = '$longitude'";
        }
        else if(!empty($locationId)){
            $query = "SELECT * FROM location_table  WHERE locationId = '$locationId'";
        }
        else{
            $query = "SELECT * FROM location_table";
        }
    
        $result = mysqli_query($con, $query);

        $i = 0;
        while($row = $result->fetch_assoc()){
            $locs[$i] = array($row);
            $i++;
        }

        mysqli_close($con);

        return $locs;
    }

    public function findNear()
    {
     //Helps report back more in depth error messages upon catching them
        $mysqli_driver = new mysqli_driver();
        $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

        //creates the connection to the database
        $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');

        // Check connection
        if (mysqli_connect_errno($con)) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $latitude= $this->latitude;
        $longitude = $this->longitude;
        $up = $longitude - 0.5;
        $down = $longitude + 0.5;
        $left = $latitude - 0.5;
        $right = $latitude + 0.5;

        $query = "SELECT * 
                  FROM location_table  
                  WHERE (CAST(latitude as DECIMAL(18,8))> '$left' AND Cast(latitude as DECIMAL(18,8))   < '$right') 
                  AND (Cast(longitude as DECIMAL(18,8))  > '$up' AND Cast(longitude as DECIMAL(18,8))  < '$down')";
    
        $result = mysqli_query($con, $query);

        $i = 0;
        while($row = $result->fetch_assoc()){
            $locs[$i] = array($row);
            $i++;
        }

        mysqli_close($con);

        return $locs;
    }

    public function del(){
            $mysqli_driver = new mysqli_driver();
            $mysqli_driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;

            $con = new mysqli('50.87.148.155', 'crivers_addict', 'Pickup2014', 'crivers_pickup_addict');
       
             // Check connection
            if (mysqli_connect_errno($con)) {
                 echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            $query = "DELETE FROM location_table";

            $result = mysqli_query($con, $query);

            mysqli_close($con);

            return $result;
    }

    public function toArray()
    {
        //return an array version of the todo item
        return array(
            /*'locationId' => $this->location_id,*/
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'houseNumber' => $this->houseNumber,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'zipcode' =>$this->zipcode,
            'description' => $this->description
        );
    }
}