<?php
class Game {
    private $_params;
     
    public function __construct($params) {
        $this->_params = $params;
    }
     
    public function createAction() {
        //create a new todo item
        $game = new GameItem();
    	$game->locationId = $this->_params['loc'];
   	    $game->sportId = $this->_params['sId'];
        $game->title = $this->_params['title'];
        $game->description = $this->_params['des'];
        $game->startTime = $this->_params['sTime'];
        $game->endTime = $this->_params['eTime'];
    	$game->minPlayers = $this->_params['minP'];
        $game->maxPlayers = $this->_params['maxP'];
        //$game->address = $this->_params['location'];
     
        //pass the user's username and password to save the new user
    	$array = $game->create();
     
        //return the game item in array format
    	return $array;
    }
     
    public function findAction() {
        //finds and returns a list of games
        $game = new GameItem();

        $game->gameId = $this->_params['gId'];
        $game->locationId = $this->_params['loc'];
        $game->sportId = $this->_params['sId'];
        $game->title = $this->_params['title'];
        $game->startTime = $this->_params['sTime'];

        $games = $game->find();

        return $games;
    }
     
    public function deleteAction() {
        //delete a game item
        $game = new GameItem();

        $game->gameId = $this->_params['gameId'];

        $success = $game->delete();

        if(!$success){
            $result = "Failed to delete game.";
        }
        else{
            $result = "Game deleted sucessfully";
        }

        return $result;
    }

    public function delAction(){
    	$game = new GameITem();

    	$success = $game->del();

       if(!$success){
            $result = "Failed to delete game.";
        }
        else{
            $result = "Game deleted sucessfully";
        }

        return $result;
    }
}