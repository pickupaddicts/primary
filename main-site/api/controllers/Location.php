<?php
class Location {
    private $_params;
     
    public function __construct($params) {
        $this->_params = $params;
    }
     
    public function createAction() {
        //create a new location item
        $loc = new LocationItem();
    	$loc->latitude = $this->_params['lat'];
   	    $loc->longitude = $this->_params['long'];
        $loc->houseNumber = $this->_params['hNum'];
        $loc->address = $this->_params['address'];
        $loc->city = $this->_params['city'];
        $loc->state = $this->_params['state'];
        $loc->zipcode = $this->_params['zip'];
   	    $loc->description = $this->_params['des'];

        //pass the user's username and password to save the new user
        if(empty($this->_params['address'])){
    	    $array = $loc->createAddr();
        }
        else{
            $array = $loc->createCoor();
        }

        //return the todo item in array format
    	return $array;
    }
     
    public function findAction(){
        //find a nearby location item
        $loc = new LocationItem();
        $loc->locationId = $this->_params['locId'];
        $loc->latitude = $this->_params['lat'];
        $loc->longitude = $this->_params['long'];

        //invoke the find action to see if an entered location already exists
        $value = $loc->find();

        return $value;
    }

    public function findNearAction(){
        //find a nearby location item
        $loc = new LocationItem();
        $loc->latitude = $this->_params['lat'];
        $loc->longitude = $this->_params['long'];

        //invoke the find action to see if an entered location already exists
        $value = $loc->findNear();

        return $value;
    }

    public function delAction(){
        $loc = new LocationITem();

        $success = $loc->del();

       if(!$success){
            $result = "Failed to delete locations.";
        }
        else{
            $result = "Locations deleted sucessfully.";
        }

        return $result;
    }
}