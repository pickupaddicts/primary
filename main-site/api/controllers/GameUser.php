<?php
class GameUser {
    private $_params;
     
    public function __construct($params) {
        $this->_params = $params;
    }
     
    public function createAction() {
        //create a new todo item
        $gameUser = new GameUserItem();
    	$gameUser->gameId = $this->_params['gameId'];
   	    $gameUser->userId = $this->_params['userId'];
     
        //pass the user's username and password to save the new user
    	$array = $gameUser->create();
     
        //return the game item in array format
    	return $array;
    }
     
    public function findAction() {
        //finds and returns a list of games
        $gameUser = new GameUserItem();

        $gameUser->gameId = $this->_params['gameId'];
        $gameUser->userId = $this->_params['userId'];

        $gameUsers = $gameUser->find();

        return $gameUsers;
    }
     
    public function deleteAction() {
        //delete a game item
        $gameUser = new GameUserItem();

        $gameUser->gameId = $this->_params['gameId'];
        $gameUser->userId = $this->_params['userId'];

        $success = $gameUser->delete();

        if(!$success){
            $result = "Failed to delete game user.";
        }
        else{
            $result = "Game user deleted sucessfully.";
        }

        return $result;
    }

    public function delAction(){
    	$gameUser = new GameUserItem();

    	$success = $gameUser->del();

       if(!$success){
            $result = "Failed to delete game.";
        }
        else{
            $result = "Game deleted sucessfully";
        }

        return $result;
    }
}