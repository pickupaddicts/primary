<?php
class User {
    private $_params;
     
    public function __construct($params) {
        $this->_params = $params;
    }
     
    public function createAction() {
        //create a new user item
        $user = new UserItem();
    	$user->email = $this->_params['email'];
   	    $user->password = $this->_params['password'];
   	    $user->firstName = $this->_params['firstName'];
    	$user->lastName = $this->_params['lastName'];
        $user->entryDate = $this->_params['entry'];
        $user->deleteDate = $this->_params['delete'];
        $user->birthday = $this->_params['bday'];
        $user->gender = $this->_params['gen'];
        $user->phoneNumber = $this->_params['phone'];
        $user->address1 = $this->_params['add1'];
        $user->address2 = $this->_params['add2'];
        $user->city = $this->_params['city'];
        $user->state = $this->_params['state'];
        $user->country = $this->_params['country'];
        $user->zipCode = $this->_params['zip'];
     
        //pass the user's username and password to save the new user
    	$array = $user->create($this->_params['email'], $this->_params['password']);

        //return the todo item in array format
    	return $array;
    }
     

    public function loginAction(){
        //login to a user's account
        $user = new UserItem();

        $user->email = $this->_params['email'];
        $user->password = $this->_params['password'];

        //pass the user's username and password to authenticate the user
        $info = $user->login($this->_params['email'], $this->_params['password']);

        return $info;
    }
    public function confirmAction() {
        //confirm a user item
    	$user = new UserItem();

    	$user->email = $this->_params['userId'];

        $data = $user->confirm();

        return $data;
    }

    public function checkAction(){
    	//check to see if a user is confirmed
    	$user = new UserItem();

    	$user->email = $this->_params['email'];
    	$user->password = $this->_params['password'];

    	$data = $user->check($this->_params['email'], $this->_params['password']);

    	return $data;
    }
     
    public function updateAction() {
        //update a user item
        $user = new UserItem();

        $user->email = $this->_params['email'];
        $user->password = $this->_params['password'];
        $user->firstName = $this->_params['firstName'];
        $user->lastName = $this->_params['lastName'];

        $user->publicId = $user->update($this->_params['email'], $this->_params['password']);

        return $user->publicId;
    }
     
    public function deleteAction() {
        //delete a user item
        $user = new UserItem();

        $user->email = $this->_params['email'];
        $user->password = $this->_params['password'];

        $success = $user->delete($this->_params['email'], $this->_params['password']);

        if(!$success){
            $result = "Failed to delete user.";
        }
        else{
            $result = "User deleted sucessfully";
        }

        return $result;
    }

    public function showAction()
    {
    	$user = new UserItem();

    	$users = $user->show();

    	return $users;
    }

        public function delAction(){
        $user = new UserITem();

        $success = $user->del();

       if(!$success){
            $result = "Failed to delete user.";
        }
        else{
            $result = "User deleted sucessfully";
        }

        return $result;
    }
}