<?php
class Sport{
    private $_params;
     
    public function __construct($params) {
        $this->_params = $params;
    }
     
    public function createAction() {
        //create a new location item
        $sport = new SportItem();
    	$sport->name = $this->_params['name'];

        //pass the user's username and password to save the new user
    	$result = $sport->create();

        //return the todo item in array format
    	return $result;
    }
     
    public function showAction(){
        //find a nearby location item
        $sport = new SportItem();
 
        //invoke the find action to see if an entered location already exists
        $sports = $sport->show();

        return $sports;
    }
}