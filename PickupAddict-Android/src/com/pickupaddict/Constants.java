package com.pickupaddict;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.EditText;

public class Constants 
{
	final public static String APP_NAME = "com.pickupaddict";
	final public static String USER = "com.pickupaddict.USER";
	final public static String GAME = "com.pickupaddict.GAME";
	final public static String GAMES = "com.pickupaddict.GAMES";
	final public static String EMAIL = "com.pickupaddict.EMAIL"; 
	final public static String PASSWORD = "com.pickupaddict.PASSWORD"; 
	final public static String NAME = "com.pickupaddict.NAME"; 
	final public static String ADDRESS = "com.pickupaddict.ADDRESS"; 
	final public static String DESCRIPTION = "com.pickupaddict.DESCRIPTION"; 
	final public static String START_DATE = "com.pickupaddict.START_DATE"; 
	final public static String END_DATE = "com.pickupaddict.END_DATE"; 
	final public static String FIRST_NAME = "com.pickupaddict.FIRST_NAME"; 
	final public static String LAST_NAME = "com.pickupaddict.LAST_NAME"; 
	final public static String IS_NEW_USER = "com.pickupaddict.IS_NEW_USER";
	final public static String IS_NEW_LOCATION = "com.pickupaddict.IS_NEW_LOCATION";
	final public static String SHOW_MAP_INSTRUCTION_DIALOG = "com.pickupaddict.SHOW_MAP_INSTRUCTION_DIALOG";
	final public static SimpleDateFormat DATE_TIME_PARSE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final public static SimpleDateFormat DATE_TIME_CONVERT_FORMAT = new SimpleDateFormat("yyyy-MM-dd'+'HH:mm:ss");
	
	
	public static SharedPreferences GetSharedPrefs(Context context) 
	{
		return context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
	}
	
	
	public static AlertDialog GetAlertDialogOK(Context context, String title, String message) 
	{
		AlertDialog.Builder dialog = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
		dialog.setTitle(title);
	    dialog.setMessage(message);
	    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	dialog.cancel();
	        }
	     });
	    AlertDialog d = dialog.create();
	    return d;
	}
}
