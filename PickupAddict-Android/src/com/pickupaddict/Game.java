package com.pickupaddict;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;


public class Game implements Serializable {

	private static final long serialVersionUID = 1L;
	private Sport sport;
	private int gameId;
	private String name;
	private String description;
	private Date startTime;
	private Date endTime;
	private Location location;
	private int minimumNumberPlayers;
	private int maximumNumberPlayers;
	private ArrayList<Player> players;
	private ArrayList<Equipment> equipment;
	
	
	public Game() 
	{
		this.location = new Location();
		this.sport = new Sport();
	}
	
	public Game(JSONObject json)
	{
		this.location = new Location();
		this.sport = new Sport();
		try 
		{
			this.gameId = json.getInt("gameId");
			this.location.setLocationId(json.getInt("locationId"));
			this.sport.setSportId(json.getInt("sportId"));
			this.name = json.getString("title");
			this.description = json.getString("description");
			this.startTime = Constants.DATE_TIME_PARSE_FORMAT.parse(json.getString("startTime"));
			this.endTime = Constants.DATE_TIME_PARSE_FORMAT.parse(json.getString("endTime"));
			this.minimumNumberPlayers = json.getInt("minPlayers");
			this.maximumNumberPlayers = json.getInt("maxPlayers");
		} 
		catch (Exception ex) { }
	}
	
	
	public boolean hasBeenPlayed() 
	{
		return new Date().compareTo(endTime) > 0;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public int getGameId()
	{
		return gameId;
	}

	public void setGameId(int gameId)
	{
		this.gameId = gameId;
	}

	public Sport getSport() {
		return sport;
	}


	public void setSport(Sport sport) {
		this.sport = sport;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	
	public Date getStartTime()
	{
		return startTime;
	}


	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}


	public Date getEndTime()
	{
		return endTime;
	}


	public void setEndTime(Date endTime)
	{
		this.endTime = endTime;
	}


	public int getMinimumNumberPlayers() {
		return minimumNumberPlayers;
	}


	public void setMinimumNumberPlayers(int minimumNumberPlayers) {
		this.minimumNumberPlayers = minimumNumberPlayers;
	}


	public int getMaximumNumberPlayers() {
		return maximumNumberPlayers;
	}


	public void setMaximumNumberPlayers(int maximumNumberPlayers) {
		this.maximumNumberPlayers = maximumNumberPlayers;
	}


	public ArrayList<Player> getPlayers() {
		return players;
	}


	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}


	public ArrayList<Equipment> getEquipment() {
		return equipment;
	}


	public void setEquipment(ArrayList<Equipment> equipment) {
		this.equipment = equipment;
	}
	
	
}
