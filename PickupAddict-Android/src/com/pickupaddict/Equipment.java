package com.pickupaddict;

public class Equipment
{

	private String name;
	private String description;
	private boolean isRequired;

	
	public Equipment()
	{

	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public String getDescription()
	{
		return description;
	}


	public void setDescription(String description)
	{
		this.description = description;
	}


	public boolean isRequired()
	{
		return isRequired;
	}


	public void setRequired(boolean isRequired)
	{
		this.isRequired = isRequired;
	}
	
	
}
