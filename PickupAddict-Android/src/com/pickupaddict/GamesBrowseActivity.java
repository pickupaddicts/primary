package com.pickupaddict;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class GamesBrowseActivity extends Activity {

	
    @Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_browse_games);
        initComponents();
	}


	private void initComponents() 
    {
    	Button btnViewList = (Button) this.findViewById(R.id.browse_button_list);
    	btnViewList.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				Intent i = new Intent(getApplicationContext(), GamesBrowseListActivity.class);
				startActivity(i);
			}
    	});

    	Button btnViewMap = (Button) this.findViewById(R.id.browse_button_map);
    	btnViewMap.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				Intent i = new Intent(getApplicationContext(), GamesBrowseMapActivity.class);
				startActivity(i);
			}
    	});
    	
    	Spinner sportSpinner = (Spinner) this.findViewById(R.id.browse_spinner_sport);
    	
    	String[] sports = {"All", "Basketball", "Football", "Baseball", "Tennis"};
        ArrayAdapter sportsAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, sports);
        sportSpinner.setAdapter(sportsAdapter);
        
        Spinner locSpinner = (Spinner) this.findViewById(R.id.browse_spinner_location);
    	
    	String[] locItems = {"My Location", "Manual Lookup"};
        ArrayAdapter locAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, locItems);
        locSpinner.setAdapter(locAdapter);
    }
}