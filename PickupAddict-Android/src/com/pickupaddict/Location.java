package com.pickupaddict;

import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class Location
{
	private int locationId, zip;
	private LatLng latLng;
	private String houseNumber, street, city, state, description;
	
	public Location() {
		
	}
	
	public Location(JSONObject json) 
	{
		try 
		{
			this.locationId = json.getInt("locationId");
			this.latLng = new LatLng(json.getDouble("latitude"), json.getDouble("longitude"));
			this.houseNumber = json.getString("houseNumber");
			this.street = json.getString("address");
			this.city = json.getString("city");
			this.state = json.getString("state");
			this.zip = json.getInt("zipcode");
			this.description = json.getString("description");
		} 
		catch (Exception ex) { }
	}

	public int getLocationId()
	{
		return locationId;
	}

	public void setLocationId(int locationId)
	{
		this.locationId = locationId;
	}

	public int getZip()
	{
		return zip;
	}

	public void setZip(int zip)
	{
		this.zip = zip;
	}

	public LatLng getLatLng()
	{
		return latLng;
	}

	public void setLatLng(LatLng latLng)
	{
		this.latLng = latLng;
	}

	public String getHouseNumber()
	{
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber)
	{
		this.houseNumber = houseNumber;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
