package com.pickupaddict;

import org.json.JSONObject;

public class Sport 
{
	private String name;
	private int sportId;
	
	
	public Sport() { }
	
	public Sport(String str) {
		name = str;
	}
	
	public Sport(JSONObject json) {
		try 
		{
			this.name = json.getString("name");
			this.sportId = json.getInt("sportId");
			
		} catch (Exception ex) { }
	}
	
	
	
	public String toString() {
		return name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getSportId()
	{
		return sportId;
	}

	public void setSportId(int sportId)
	{
		this.sportId = sportId;
	}
}
