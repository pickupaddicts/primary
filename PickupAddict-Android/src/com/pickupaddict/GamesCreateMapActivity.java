package com.pickupaddict;

import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class GamesCreateMapActivity extends Activity 
							implements OnInfoWindowClickListener, OnMapLongClickListener, OnMyLocationChangeListener {
	
	static final LatLng DOWNTOWN = new LatLng(42.956398, -85.670470);
	static final LatLng CAMPUS_WEST = new LatLng(42.962919, -85.905647);
	static final LatLng PLACE1 = new LatLng(43.009123, -85.906757);
	
	private boolean locationSet = false;
	private GoogleMap map;
	private Marker newMarker;
	private Game game;
	private com.pickupaddict.Location newLocation; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse_games_map);
		
		game = new Game();
		
		Intent intent = this.getIntent();
		if (intent != null)
			game = (Game) intent.getSerializableExtra(Constants.GAME);
		
		initMap();
	}
	

	private void initMap() 
	{
	    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	    map.setOnInfoWindowClickListener(this);
	    map.setOnMapLongClickListener(this);
	    map.setOnMyLocationChangeListener(this);
	    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	    map.setMyLocationEnabled(true);
	    map.getUiSettings().setZoomControlsEnabled(true);
	    map.getUiSettings().setMyLocationButtonEnabled(true);
	    map.getUiSettings().setCompassEnabled(true);
	    map.getUiSettings().setRotateGesturesEnabled(true);
	    map.getUiSettings().setZoomGesturesEnabled(true);
	}


	@Override
	public void onInfoWindowClick(Marker marker) 
	{

    	Intent intent = new Intent(getApplicationContext(), GameDetailsActivity.class);
    	intent.putExtra(Constants.GAME, game);
		
		// See if a new marker has been created but not handled yet
		if (newMarker != null) 
		{
			if (marker != newMarker) 
				newMarker.remove();
			else 
			{
				new CreateLocationAndGame(marker.getPosition(), game.getDescription()).execute();
				marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			}
			newMarker = null;
		}
    	startActivity(intent);
	}


	@Override
	public void onMapLongClick(LatLng loc)
	{
		// if a temp marker is already on the map, remove it. 
		if (newMarker != null)
			newMarker.remove();
		
		newMarker = map.addMarker(new MarkerOptions()
    	.position(loc)
        .title("Use This Location?")
        .snippet("Touch here to create your game!")
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
		newMarker.showInfoWindow();
	}


	@Override
	public void onMyLocationChange(Location location)
	{
		if (!locationSet)
		{
		    LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
		    map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 8));
		    map.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
		    locationSet = true;
		}
	}

	
	private boolean CreateGame()
	{
		JSONObject json = DataAccess.CreateGame(game);
		
		try
		{
			return json.getBoolean("success");
		} 
		catch (Exception e) 
		{ 
			Constants.GetAlertDialogOK(GamesCreateMapActivity.this, "Oops!", "An error occured creating the game")
				.show();
			return false;
		}
	}


	private class CreateLocationAndGame extends AsyncTask <Void, Void, JSONObject> 
	{
		private LatLng loc;
		private String desc;
		
		private CreateLocationAndGame(LatLng loc, String description)
		{
			this.loc = loc;
			this.desc = description;
		}
		
		public JSONObject doInBackground(Void... params)
		{
			return DataAccess.CreateLocation(loc, desc);
		}
		
		protected void onPostExecute(JSONObject json)
		{
			try
			{
				if(json.getBoolean("success"))
				{
					newLocation = new com.pickupaddict.Location(json.getJSONObject("data"));
					CreateGame();
				}
				else
				{
					throw new Exception();
				}
			} catch (Exception e)
			{
				Constants.GetAlertDialogOK(GamesCreateMapActivity.this, "Oops!", "An error occured setting the game location")
				.show();
			}
		}
	}
}