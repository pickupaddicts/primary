package com.pickupaddict;

import java.io.Serializable;

public class User implements Serializable {

	private String email, firstName, lastName, password;
	private boolean isConfirmed;
	
	public User() {
		email = "";
		firstName = "";
		lastName = "";
		password = "";
		isConfirmed = false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public boolean isConfirmed()
	{
		return isConfirmed;
	}

	public void setConfirmed(boolean isConfirmed)
	{
		this.isConfirmed = isConfirmed;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	
	
}
