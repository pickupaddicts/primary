package com.pickupaddict;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class GamesBrowseMapActivity extends Activity implements OnInfoWindowClickListener {
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd", Locale.getDefault());
	private SimpleDateFormat dayDateFormat = new SimpleDateFormat("c, MMM dd", Locale.getDefault());
	private SimpleDateFormat timeFormat = new SimpleDateFormat("H:mma", Locale.getDefault());
	
	private GoogleMap map;
	private ArrayList<Game> games = null;
	private boolean locationSet = false;


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse_games_map);
		
		initMap();
		
		Intent intent = this.getIntent();
		if (intent != null) 
			games = (ArrayList<Game>) intent.getSerializableExtra(Constants.GAMES);
		
		if (games == null)
			new RetrieveGamesAsync().execute();
		
	}
	
	
	private void initMap() 
	{
	    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	    map.setOnInfoWindowClickListener(this);
	    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	    map.setMyLocationEnabled(true);
	    map.getUiSettings().setZoomControlsEnabled(true);
	    map.getUiSettings().setMyLocationButtonEnabled(true);
	    map.getUiSettings().setCompassEnabled(true);
	    map.getUiSettings().setRotateGesturesEnabled(true);
	    map.getUiSettings().setZoomGesturesEnabled(true);
	    map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			public void onMyLocationChange(Location location)
			{
				if (!locationSet)
				{
				    LatLng loc = new LatLng(location.getLatitude(),location.getLongitude());
				    map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 8));
				    map.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
				    locationSet = true;
				}
			}
	    });
	    
	    
	    map.addMarker(new MarkerOptions()
	    		.position(new LatLng(42.963557, -85.904066)).title("Sample Game").snippet("6:00PM - 8:30PM")
	    		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
	    
	    map.addMarker(new MarkerOptions()
		.position(new LatLng(42.967899, -85.892114)).title("Another Game").snippet("6:00PM - 8:30PM")
		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
	}
	
	
	/**
	 * Add a marker to the map
	 */
	private void addMarker(Game game)
	{
		try 
		{
			Random r = new Random();
		    map.addMarker(new MarkerOptions()
		    	.position(new LatLng(42.962919 + r.nextFloat()/10, -85.905647 + r.nextFloat()/10))
		        .title(game.getName())
		        .snippet(dayDateFormat.format(game.getStartTime()) + "   "
		        		+ timeFormat.format(game.getStartTime()) + "-" 
		        		+ timeFormat.format(game.getEndTime()))
		        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
		}
		catch (Exception ex) { }
	}
	

	/**
	 * On Info Window Click Listener
	 */
	public void onInfoWindowClick(Marker marker) 
	{
    	Intent intent = new Intent(getApplicationContext(), GameDetailsActivity.class);
    	for (Game g : games)
    	{
    		if (marker.getPosition().equals(g.getLocation().getLatLng()))
    		{
    			intent.putExtra(Constants.GAME, g);
    	    	startActivity(intent);
    		}
    	}
	}
	
	
	/**
	 * Retrieve a list of all games and add them to the map
	 *
	 */
	private class RetrieveGamesAsync extends AsyncTask <Void, Void, JSONObject> 
	{
		public JSONObject doInBackground(Void... params) 
		{
			return DataAccess.GetAllGames();
		}
		
		protected void onPostExecute(JSONObject json)
		{
			try
			{
				if(json.getBoolean("success"))
				{
					games = new ArrayList<Game>();
					JSONArray data1 = json.getJSONArray("data");
					
					for (int i = 0; i < data1.length(); i++)
					{
						JSONArray data2 = data1.getJSONArray(i);
						Game g = new Game(data2.getJSONObject(0)); 
						games.add(g);
						addMarker(g);
					}
				}
				else throw new Exception();
			} 
			catch (Exception e)
			{
				Constants.GetAlertDialogOK(GamesBrowseMapActivity.this, 
						"Oops!", "An error occured communicating with the server.")
				.show();
			}
		}
	}
}