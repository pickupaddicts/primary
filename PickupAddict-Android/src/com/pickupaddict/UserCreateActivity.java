package com.pickupaddict;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserCreateActivity extends Activity {
	
	private EditText editText_email;
	private EditText editText_firstName;
	private EditText editText_lastName;
	private EditText editText_password1;
	private EditText editText_password2;
	
	/**
	 * On Create
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_create);
        
	    initComponents();
	    Intent intent = this.getIntent();
	    if (intent != null) {
	    	this.editText_email.setText(intent.getStringExtra(Constants.EMAIL));
	    }
	    
	}
	
	
	/**
	 * Initialize Components
	 */
	private void initComponents() {

		editText_email = (EditText) this.findViewById(R.id.editText_new_email);
		editText_firstName = (EditText) this.findViewById(R.id.editText_firstName);
		editText_lastName = (EditText) this.findViewById(R.id.editText_lastName);
		editText_password1 = (EditText) this.findViewById(R.id.editText_password);
		editText_password2 = (EditText) this.findViewById(R.id.editText_password_repeat);
		
		Button button_createAccount = (Button) this.findViewById(R.id.btn_CreateAccount);
		button_createAccount.setOnClickListener(new OnClickListener() {
			public void onClick(View v) 
			{
				hideKeyboard();
				
				// Make sure all fields are non-empty
				if (editText_email.getText().toString().isEmpty() || 
					editText_firstName.getText().toString().isEmpty() || 
					editText_lastName.getText().toString().isEmpty() || 
					editText_password1.getText().toString().isEmpty() || 
					editText_password2.getText().toString().isEmpty())
				{
					Constants.GetAlertDialogOK(UserCreateActivity.this, "Missing Fields", "All information is required!")
					.show();
					return;
				}
				
				// Check for valid email
				if (!editText_email.getText().toString().contains("@") ||
					!editText_email.getText().toString().contains(".")) 
				{
					Constants.GetAlertDialogOK(UserCreateActivity.this, "Invalid Email Address", "A valid email address is required")
					.show();
					return;
				}
				
				// Make sure password is at least 6 characters
				if (editText_password1.getText().toString().length() < 6) 
				{
					Constants.GetAlertDialogOK(UserCreateActivity.this, "Invalid Password", "Password must be at least 6 characters")
					.show();
					return;
				}
				
				// Make sure passwords are equal
				if (!editText_password1.getText().toString().equals(editText_password2.getText().toString())) 
				{
					Constants.GetAlertDialogOK(UserCreateActivity.this, "Invalid Password", "Passwords do not match")
					.show();
					return;
				}
				
				// Execute the API call
				try 
				{
					new CreateUser(editText_email.getText().toString(), 
								   editText_password1.getText().toString(),
								   editText_firstName.getText().toString(), 
								   editText_lastName.getText().toString()).execute();
				} 
				catch (Exception ex) 
				{
					Constants.GetAlertDialogOK(UserCreateActivity.this, "Oops!", "Failed to reach the server. Please check that "
							+ "you have a valid network connection")
					.show();
				}
			}
		});
	}
	
	
	
	
	private class CreateUser extends AsyncTask <Void, Void, JSONObject> 
	{
		private String email, first, last, pass;
		private RelativeLayout layout;
		
		CreateUser(String email, String first, String last, String pass) {
			this.email = email;
			this.first = first;
			this.last = last;
			this.pass = pass;
			layout = (RelativeLayout) findViewById(R.id.user_create_progress_relativeLayout);
			layout.setVisibility(RelativeLayout.VISIBLE);
		}
		
		public JSONObject doInBackground(Void... params) 
		{
			return DataAccess.UserCreate(email, first, last, pass);
		}
		
		protected void onPostExecute(JSONObject json) 
		{
			try
			{
				if (json.getBoolean("success"))
				{
					JSONObject data = json.getJSONObject("data");
					Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
					intent.putExtra(Constants.IS_NEW_USER, true);
					intent.putExtra(Constants.EMAIL, data.getString("email"));
					intent.putExtra(Constants.FIRST_NAME, data.getString("firstName"));
					intent.putExtra(Constants.LAST_NAME, data.getString("lastName"));
					startActivity(intent);
					finish();
				}
				else 
				{
					layout.setVisibility(RelativeLayout.GONE);
					if (json.getString("errormsg").toLowerCase().contains("duplicate entry")) 
					{
						Constants.GetAlertDialogOK(UserCreateActivity.this, "Account Exists","An account with this email address already exists!")
						.show();
					}
					else {
						Constants.GetAlertDialogOK(UserCreateActivity.this, "Oops!","An error occured creating your account. Please try again!")
						.show();
					}
				}
			} catch (Exception e) 
			{
				layout.setVisibility(RelativeLayout.GONE);
				Constants.GetAlertDialogOK(UserCreateActivity.this, "Oops!","An error occured communicating with the server. Check that you have a valid internet connection!")
				.show();
			}
		}
	}
	

	/** 
	 * Hide Keyboard
	 */
	private void hideKeyboard() {
	    InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

	    // check if no view has focus:
	    View view = this.getCurrentFocus();
	    if (view != null) {
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
}