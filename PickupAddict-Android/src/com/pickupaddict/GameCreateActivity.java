package com.pickupaddict;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TimePicker;

public class GameCreateActivity extends Activity
{
	private EditText title, description, date, startTime, endTime, minPlayers, maxPlayers, address;
	private Spinner sportSpinner;
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
	private final SimpleDateFormat timeFormat = new SimpleDateFormat("K:mm a");
	
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        
        getActionBar().setHomeButtonEnabled(true);
        
        initComponents();
	}
	
	private void initComponents() 
	{
		sportSpinner = (Spinner) findViewById(R.id.createGame_spinner_sport);

		String[] sports = new String[] {"Basketball", "Football", "Soccer"};
        ArrayAdapter sportsAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spinner_item, R.id.spinner_textView, sports);
        sportSpinner.setAdapter(sportsAdapter);
		
		title = (EditText) findViewById(R.id.createGame_title);
		description = (EditText) findViewById(R.id.createGame_description);
		minPlayers = (EditText) findViewById(R.id.createGame_minPlayers);
		maxPlayers = (EditText) findViewById(R.id.createGame_maxPlayers);
		date = (EditText) findViewById(R.id.createGame_date);
		date.setText(dateFormat.format(Calendar.getInstance().getTime()));
		startTime = (EditText) findViewById(R.id.createGame_startTime);
		Calendar cal = Calendar.getInstance();
		startTime.setText(timeFormat.format(cal.getTime()));
		endTime = (EditText) findViewById(R.id.createGame_endTime);
		cal.add(Calendar.HOUR, 1);
		endTime.setText(timeFormat.format(cal.getTime()));
		date.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				try 
				{
					Calendar cal = Calendar.getInstance();
					cal.setTime(dateFormat.parse(date.getText().toString()));
					new DatePickerDialog(GameCreateActivity.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, dateSetListener, 
							cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
						.show();
				}
				catch (Exception ex) { }
			}
		});
		startTime.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				try 
				{
					Calendar cal = Calendar.getInstance();
					cal.setTime(timeFormat.parse(startTime.getText().toString()));
					new TimePickerDialog(GameCreateActivity.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, startTimeSetListener, 
							cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false)
						.show();
				}
				catch (Exception ex) { }
			}
		});
		endTime.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				try 
				{
					Calendar cal = Calendar.getInstance();
					cal.setTime(timeFormat.parse(endTime.getText().toString()));
					new TimePickerDialog(GameCreateActivity.this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, endTimeSetListener, 
							cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false)
						.show();
				}
				catch (Exception ex) { }
			}
		});
		
		Button locationBtn = (Button) findViewById(R.id.createGame_button_chooseLocation);
		locationBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyyHH:mm a");
				try 
				{
					Game game = new Game();
					game.setName(title.getText().toString());
					game.setDescription(description.getText().toString());
					game.setSport(((Sport) sportSpinner.getSelectedItem()));
					game.setStartTime(df.parse(date.getText().toString() + startTime.getText().toString()));
					game.setEndTime(df.parse(date.getText().toString() + endTime.getText().toString()));
					game.setMinimumNumberPlayers(Integer.parseInt(minPlayers.getText().toString()));
					game.setMaximumNumberPlayers(Integer.parseInt(maxPlayers.getText().toString()));

					AlertDialog.Builder dialog = new AlertDialog.Builder(GameCreateActivity.this, AlertDialog.THEME_HOLO_DARK);
					dialog.setTitle("Location Type");
					dialog.setMessage("How would you like to set the game location?");
					dialog.setPositiveButton("Enter an address", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) 
						{
							Context context = GameCreateActivity.this;
							AlertDialog.Builder alert = new AlertDialog.Builder(context);

							alert.setTitle("Title");
							alert.setMessage("Message");

							// Set an EditText view to get user input 
							final EditText input = new EditText(context);
							alert.setView(input);

							alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) 
								{
									String value = input.getText().toString();
									Intent intent = new Intent(getApplicationContext(), GameDetailsActivity.class);
									//DataAccess.CreateLocation(number, street, city, state, zip, desc)

								}
							});

							alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									// Canceled.
								}
							});

							alert.show();
						}
					});
					dialog.setNegativeButton("Choose on map", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) { 
							Intent intent = new Intent(getApplicationContext(), GamesCreateMapActivity.class);
							startActivity(intent);
						}
					});
					dialog.create().show();
				} 
				catch (Exception ex) 
				{ 
					Constants.GetAlertDialogOK(GameCreateActivity.this, "Missing Data",	"All fields are required!");
				}
			}
		});
	}
	

	private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() 
	{
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			date.setText(dateFormat.format(cal.getTime()));
		}
	};
	
	private TimePickerDialog.OnTimeSetListener startTimeSetListener = new TimePickerDialog.OnTimeSetListener()
	{
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
			cal.set(Calendar.MINUTE, minute);
			startTime.setText(timeFormat.format(cal.getTime()));
		}
	};
	
	private TimePickerDialog.OnTimeSetListener endTimeSetListener = new TimePickerDialog.OnTimeSetListener()
	{
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
			cal.set(Calendar.MINUTE, minute);
			endTime.setText(timeFormat.format(cal.getTime()));
		}
	};
}