package com.pickupaddict;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Dictionary;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.VisibleRegion;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class UserLoginActivity extends Activity {

	private EditText editText_email;
	private EditText editText_password;
	private ProgressBar progressBar;
	
	/**
	 * On Create
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        
        getActionBar().setTitle("Log In");
        initComponents();
	}
	
	/**
	 * Initialize Components
	 */
	private void initComponents() 
	{	
		Button btnLogin = (Button) this.findViewById(R.id.btn_LogIn);
		btnLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try {
					hideKeyboard();
					new ValidateCredentials(editText_email.getText().toString(), editText_password.getText().toString()).execute();
				}
				catch (Exception ex) 
				{
					Constants.GetAlertDialogOK(UserLoginActivity.this, 
							"Login Failed", 
							"An error occurred communicating with the server.")
					.show();
				}
			}
		});
		
		Button btnNewUser = (Button) this.findViewById(R.id.btn_NewUser);
		btnNewUser.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				Intent intent = new Intent(getApplicationContext(), UserCreateActivity.class);
				intent.putExtra(Constants.EMAIL, editText_email.getText().toString());
				startActivity(intent);
			}
		});
		
		editText_email = (EditText) this.findViewById(R.id.editText_email);
		editText_email.setText("olesakn@gmail.com");
		
		editText_password = (EditText) this.findViewById(R.id.editText_password);
		editText_password.setText("helloo");
	}
	
	
	private class ValidateCredentials extends AsyncTask <Void, Void, JSONObject> 
	{
		private String email, password;
		private RelativeLayout layout;
		
		ValidateCredentials(String email, String password) {
			this.email = email;
			this.password = password;

			layout = (RelativeLayout) findViewById(R.id.user_login_progress_relativeLayout);
			layout.setVisibility(RelativeLayout.VISIBLE);
		}
		
		public JSONObject doInBackground(Void... params) 
		{
			return DataAccess.UserLogin(email, password);
		}
		
		protected void onPostExecute(JSONObject json) 
		{
			try
			{
				if(json.getBoolean("success"))
				{
					JSONObject data = json.getJSONObject("data");
					Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
					intent.putExtra(Constants.EMAIL, editText_email.getText().toString());
					intent.putExtra(Constants.FIRST_NAME, data.getString("firstName"));
					intent.putExtra(Constants.LAST_NAME, data.getString("lastName"));
					startActivity(intent);
					finish();
				}
				else 
				{
					layout.setVisibility(RelativeLayout.GONE);
					Constants.GetAlertDialogOK(UserLoginActivity.this, 
							"Login Failed", 
							"Unable to verify your credentials. Check your spelling!")
					.show();
				}
			} catch (Exception e)
			{
				layout.setVisibility(RelativeLayout.GONE);
				Constants.GetAlertDialogOK(UserLoginActivity.this, 
						"Login Failed", 
						"An error occurred communicating with the server. Check your internet connection and try again.")
				.show();
			}
		}
	}
	
	
	/** 
	 * Hide Keyboard
	 */
	private void hideKeyboard() {
	    InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

	    // check if no view has focus:
	    View view = this.getCurrentFocus();
	    if (view != null) {
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
}
