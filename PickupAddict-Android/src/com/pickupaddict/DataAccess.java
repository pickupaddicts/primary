package com.pickupaddict;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.os.AsyncTask;

public class DataAccess {
	
	public static JSONObject UserLogin(String email, String password)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=user&action=login"
			+ createParam("email", email)
			+ createParam("password", password));
	}
	
	
	public static JSONObject UserCreate(String email, String password, String first, String last)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=user&action=create"
			+ createParam("email", email)
			+ createParam("password", password)
			+ createParam("firstName", first)
			+ createParam("lastName", last));
	}
	
	
	public static JSONObject GetAllSports() 
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=sport&action=show");
	}
	
	
	public static JSONObject CreateGame(Game game)
	{
		return CreateGame(game.getLocation().getLocationId(), game.getSport(), game.getName(), game.getDescription(), game.getStartTime(), 
					game.getEndTime(), game.getMinimumNumberPlayers(), game.getMaximumNumberPlayers());
	}
	

	public static JSONObject CreateGame(int locId, Sport sport, String title, String desc, Date start, Date end, int minPlayers, int maxPlayers)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=game&action=create"
			+ createParam("loc", locId)
			+ createParam("sId", sport)
			+ createParam("title", title)
			+ createParam("des", desc)
			+ createParam("sTime", start)
			+ createParam("eTime", end)
			+ createParam("minP", minPlayers)
			+ createParam("maxP", maxPlayers));
	}
	
	
	public static JSONObject DeleteGame(Game game, String email)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=gameUser&action=create"
			+ createParam("gameId", game) // this isn't in the API YET!!!!!
			+ createParam("userId", email)
			+ createParam("entry", Calendar.getInstance().getTime()));
	}
	
	
	
	public static JSONObject DeleteGame(Game game)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict/api/?controller=game&action=delete"
			+ createParam("gameId", game.getGameId()));
	}
	
	
	public static JSONObject GetAllGames() 
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=game&action=find");
		//return GetGames(null, null, null, null);//Calendar.getInstance().getTime());
	}
	
	
	
	public static JSONObject GetGames(String loc, Sport sport, String title, Date start)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=game&action=find"
			+ createParam("loc", loc)
			+ createParam("sId", sport)	+ createParam("title", title)
			+ createParam("sTime", start));	
	}
	
	public static JSONObject AddPlayerToGame(Game game, String email)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=gameUser&action=create"
			+ createParam("gameId", game)
			+ createParam("userId", email)
			+ createParam("entry", Calendar.getInstance().getTime()));
	}
	
	public static JSONObject FindPlayersForGame(Game game)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=gameUser&action=find&gameId=" + game.getGameId());
	}
	
	
	public static JSONObject FindGamesForPlayer(String email)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=gameUser&action=find&userId=" + email);
	}
	
	
	public static JSONObject RemovePlayerFromGame(Game game, String email)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=gameUser&action=delete"
			+ createParam("gameId", game)
			+ createParam("userId", email));
	}
	
	public static JSONObject CreateLocation(LatLng latLng, String desc)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=location&action=create"
			+ createParam("lat", latLng.longitude)
			+ createParam("long", latLng.longitude)
			+ createParam("des", desc));		
	}
	
	
	public static Location CreateLocationWithReturn(String address)
	{
		return (Location) CreateFromJSON(CreateLocation(address), Location.class);
	}
	
	
	public static Object CreateFromJSON(JSONObject json, Class<?> t)
	{
		if (t.getClass() == Game.class.getClass()) 		return new Game(json);
		if (t.getClass() == Location.class.getClass()) 	return new Location(json);
		if (t.getClass() == Sport.class.getClass()) 	return new Sport(json);
		
		return null;
	}
	

	public static JSONObject CreateLocation(String address)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=location&action=create"
			+ createParam("address", address));
	}
	
	public static JSONObject CreateLocation(int number, String street, String city, String state, int zip, String desc)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=location&action=create"
			+ createParam("hNum", number)
			+ createParam("address", street)
			+ createParam("city", city)
			+ createParam("state", state)
			+ createParam("zip", zip)
			+ createParam("des", desc));
	}
	
	
	public static JSONObject GetLocation(int locId)
	{
		return ParseHttpIntoJSONObject("http://pickupaddict.com/api/?controller=location&action=find&locId=" + locId);
	}
	
	
	public static User GetUser(Context c) 
	{
		User user = new User();
		user.setEmail(Constants.GetSharedPrefs(c).getString(Constants.EMAIL, ""));
		user.setFirstName(Constants.GetSharedPrefs(c).getString(Constants.FIRST_NAME, ""));
		user.setLastName(Constants.GetSharedPrefs(c).getString(Constants.LAST_NAME, ""));
		user.setPassword(Constants.GetSharedPrefs(c).getString(Constants.PASSWORD, ""));
		return user;
	}
	
	public void SetUser(Context c, User user) 
	{

		Constants.GetSharedPrefs(c).edit().putString(Constants.EMAIL, user.getEmail());
		Constants.GetSharedPrefs(c).edit().putString(Constants.FIRST_NAME, user.getFirstName());
		Constants.GetSharedPrefs(c).edit().putString(Constants.LAST_NAME, user.getLastName());
		Constants.GetSharedPrefs(c).edit().putString(Constants.PASSWORD, user.getLastName());
	}
	

	public static void SetShowMapInstructionDialog(Context c, boolean value) {
		Constants.GetSharedPrefs(c).edit().putBoolean(Constants.SHOW_MAP_INSTRUCTION_DIALOG, value);
	}
	
	
	public static boolean ShowMapInstructionDialog(Context c) 
	{
		return Constants.GetSharedPrefs(c).getBoolean(Constants.SHOW_MAP_INSTRUCTION_DIALOG, true);
	}
	
	
	
	
	
	/*****************************************    Private Helper Methods    *******************************************/
	
	
	
	/**
	 * HTTP-GET a web URL and return the resulting JSON
	 */
	private static JSONObject ParseHttpIntoJSONObject(String url)
	{
		try 
		{
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);
			
			HttpResponse response = httpClient.execute(httpGet, new BasicHttpContext());
			HttpEntity entity = response.getEntity();
			InputStream in = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"), 8);
		    StringBuilder sb = new StringBuilder();
	
		    String line = null;
		    while ((line = reader.readLine()) != null)
		    {
		        sb.append(line + "\n");
		    }
		    return new JSONObject(sb.toString());
		}
		catch (Exception ex) { 
			return null;
		}
	}
	
	
	private static String createParam(String param, String val) 
	{
		return (val != null && val.length() > 0) ? "&" + param + "=" + val.replace(' ', '+') : "";
	}

	private static String createParam(String param, Date val) 
	{
		return (val != null && val.getTime() > 0) ? "&" + param + "=" + Constants.DATE_TIME_CONVERT_FORMAT.format(val) : "";
	}

	private static String createParam(String param, int val) 
	{
		return (val > 0) ? "&" + param + "=" + val : "";
	}

	private static String createParam(String param, Sport val) 
	{
		return (val != null) ? "&" + param + "=" + val.getSportId() : "";
	}
	
	private static String createParam(String param, Game val) 
	{
		return (val != null) ? "&" + param + "=" + val.getGameId() : "";
	}
	
	private static String createParam(String param, double val) 
	{
		return (val > 0) ? "&" + param + "=" + val : "";
	}
	
}


