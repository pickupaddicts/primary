package com.pickupaddict;

import java.util.ArrayList;

public class Player {

	private String email;
	private String firstName;
	private String lastName;
	private String location;
	private Statistics statistics;
	private ArrayList<Game> upcomingGames;
	
	public Player() {
		
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public Statistics getStatistics()
	{
		return statistics;
	}

	public void setStatistics(Statistics statistics)
	{
		this.statistics = statistics;
	}

	public ArrayList<Game> getUpcomingGames()
	{
		return upcomingGames;
	}

	public void setUpcomingGames(ArrayList<Game> upcomingGames)
	{
		this.upcomingGames = upcomingGames;
	}
}