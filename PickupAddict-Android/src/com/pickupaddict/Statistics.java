package com.pickupaddict;

import java.util.ArrayList;

public class Statistics
{

	private Sport sport;
	private ArrayList<Game> gamesPlayed;
	private int numberGamesPlayed;
	private int numberGamesWon;
	private ArrayList<Integer> ratings;
	

	public Statistics(Sport sport)
	{
		this.sport = sport;
	}

	public double getAverageRating()
	{
		double avg = 0;
		for (int r : ratings) 
		{
			avg += r;
		}
		return avg / ratings.size();
	}

	public Sport getSport()
	{
		return sport;
	}

	public void setSport(Sport sport)
	{
		this.sport = sport;
	}

	public ArrayList<Game> getGamesPlayed()
	{
		return gamesPlayed;
	}

	public void setGamesPlayed(ArrayList<Game> gamesPlayed)
	{
		this.gamesPlayed = gamesPlayed;
	}

	public int getNumberGamesPlayed()
	{
		return numberGamesPlayed;
	}

	public void setNumberGamesPlayed(int numberGamesPlayed)
	{
		this.numberGamesPlayed = numberGamesPlayed;
	}

	public int getNumberGamesWon()
	{
		return numberGamesWon;
	}

	public void setNumberGamesWon(int numberGamesWon)
	{
		this.numberGamesWon = numberGamesWon;
	}

	public ArrayList<Integer> getRatings()
	{
		return ratings;
	}

	public void setRatings(ArrayList<Integer> ratings)
	{
		this.ratings = ratings;
	}
}
