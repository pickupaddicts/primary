package com.pickupaddict;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity 
{
	
	public static User CurrentUser = new User();
	
	private ArrayList<Game> data;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
    	this.setContentView(R.layout.activity_home);
    	
        Intent intent = this.getIntent();
        if (intent != null) 
        {
        	CurrentUser.setEmail(intent.getStringExtra(Constants.EMAIL));
        	CurrentUser.setFirstName(intent.getStringExtra(Constants.FIRST_NAME));
        	CurrentUser.setLastName(intent.getStringExtra(Constants.LAST_NAME));
        	if (intent.getBooleanExtra(Constants.IS_NEW_USER, false)) 
        	{
        		Constants.GetAlertDialogOK(HomeActivity.this, "Welcome!",
        				"You're account has successfully been created! Email confirmation is required to complete "
        				+ "your new account setup. An email has been sent to you with a confirmation link.  You must confirm "
        				+ "your email address in order to gain full access to the application.")
        		.show();
        	}
        }
        
        initComponents();
	}
    

	/**
	 * Initialize Components
	 */
	private void initComponents() {
    	
    	TextView name = (TextView) findViewById(R.id.home_textView_name);
    	name.setText(CurrentUser.getFullName());
    	
    	try {
	    	ListView listView = (ListView) findViewById(R.id.home_listView);
	    	data = new ArrayList<Game>();
	    	JSONObject json = DataAccess.FindGamesForPlayer(CurrentUser.getEmail());
	    	if (json.getBoolean("success"))
	    	{
	    		JSONArray jsonData = json.getJSONArray("data");
	    		for (int i = 0; i < jsonData.length(); i++)
	    		{
	    			JSONObject gameObj = jsonData.getJSONArray(i).getJSONObject(0); 
	    			Game g = new Game();
	    			g.setGameId(gameObj.getInt("gameId"));
	    			g.setName("test " + g.getGameId());
	        		data.add(g);
	    		}
	    	}
	    	listView.setAdapter(new GamesListCustomAdapter(this, data));
	    	listView.setOnItemClickListener(new OnItemClickListener()
	    	{
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					Intent i = new Intent(getApplicationContext(), GameDetailsActivity.class);
					i.putExtra(Constants.GAME, (Game)data.get(position));
					startActivity(i);
				}
	    	});
    	}
    	catch (Exception ex) 
    	{
    		Toast.makeText(this.getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
    	}
    }
	
	
    /**
     * On Create Options Menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.home_actions, menu);
        return true;
    }
    
    
    
    

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) 
		{
		case R.id.action_create:
			startActivity(new Intent(this, GameCreateActivity.class));
			return true;
		case R.id.action_search:
			startActivity(new Intent(this, GamesBrowseActivity.class));
			return true;
		case R.id.action_map:
			startActivity(new Intent(this, GamesBrowseMapActivity.class));
			return true;
		case R.id.action_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		case R.id.action_logout:
			new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK)
				.setTitle("Confirm")
			    .setMessage("Are you sure you want to log out?")
			    .setPositiveButton("Log out", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	startActivity(new Intent(getApplicationContext(), UserLoginActivity.class));
			        	finish();
			        }
			     })
			    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	dialog.cancel();
			        }
			     })
			     .create().show();
			return true;
		
		}
		return true;
	}





	/**
	 * Games List Custom Adapter
	 */
	class GamesListCustomAdapter extends BaseAdapter {

	    Context context;
	    ArrayList<Game> data;
	    private LayoutInflater inflater = null;

	    public GamesListCustomAdapter(Context context, ArrayList<Game> data) {
	        this.context = context;
	        this.data = data;
	        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }

	    @Override
	    public int getCount() {
	        return data.size();
	    }

	    @Override
	    public Object getItem(int position) {
	        return data.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View vi = convertView;
	        if (vi == null)
	            vi = inflater.inflate(R.layout.list_row_game, null);
	        
	        TextView textName = (TextView) vi.findViewById(R.id.textView_Name);
	        textName.setText(data.get(position).getName());

	        TextView textDesc = (TextView) vi.findViewById(R.id.textView_Description);
	        textDesc.setText(data.get(position).getDescription());
	        
	        TextView textDate = (TextView) vi.findViewById(R.id.textView_date_and_time);
	        textDate.setText("10/29 from 6:30pm - 8:00pm");
	        
	        return vi;
	    }
	}
}
