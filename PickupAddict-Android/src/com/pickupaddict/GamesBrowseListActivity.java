package com.pickupaddict;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.pickupaddict.HomeActivity.GamesListCustomAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class GamesBrowseListActivity extends Activity
{
	
	private ListView listView;
	
	@Override
	protected void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.activity_browse_games_list);
		listView = (ListView) this.findViewById(R.id.listView_games);
    	ArrayList<Game> data = new ArrayList<Game>();
    	for (int i = 0; i < 10; i++) 
    	{
    		Game g = new Game();
	    	g.setName("Game " + i);
	    	g.setDescription("Description of the game " + i);
	    	data.add(g);
    	}
    	listView.setAdapter(new GamesListCustomAdapter(getApplicationContext(), data));
    	listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id)
			{
				Intent i = new Intent(getApplicationContext(), GameDetailsActivity.class);
				i.putExtra(Constants.NAME, "Game");//data.get(position).getName());
				i.putExtra(Constants.DESCRIPTION, "Description");//data.get(position).getName());
				startActivity(i);
			}
    	});
	}
	
	

	/**
	 * Games List Custom Adapter
	 */
	class GamesListCustomAdapter extends BaseAdapter {

	    Context context;
	    ArrayList<Game> data;
	    private LayoutInflater inflater = null;

	    public GamesListCustomAdapter(Context context, ArrayList<Game> data) {
	        this.context = context;
	        this.data = data;
	        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }

	    @Override
	    public int getCount() {
	        return data.size();
	    }

	    @Override
	    public Object getItem(int position) {
	        return data.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View vi = convertView;
	        if (vi == null)
	            vi = inflater.inflate(R.layout.list_row_game, null);
	        
	        TextView textName = (TextView) vi.findViewById(R.id.textView_Name);
	        textName.setText(data.get(position).getName());

	        TextView textDesc = (TextView) vi.findViewById(R.id.textView_Description);
	        textDesc.setText(data.get(position).getDescription());
	        
	        TextView textDate = (TextView) vi.findViewById(R.id.textView_date_and_time);
	        textDate.setText("10/29 from 6:30pm - 8:00pm");
	        
	        return vi;
	    }
	}
}