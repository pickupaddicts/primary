package com.pickupaddict;

import android.R.color;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyButton extends Button {

	public MyButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
	public MyButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}
	
	public MyButton(Context context) {
		super(context);
		init(null);
	}
	
	private void init(AttributeSet attrs) {
		if (!this.isInEditMode())
			setTypeface(Typeface.createFromAsset(getContext().getAssets(), "chalkduster.ttf"));
		setTextColor(getResources().getColor(R.color.white));
		
	}
}