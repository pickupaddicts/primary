package com.pickupaddict;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class GameDetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_details);
		
		Intent intent = this.getIntent();
		if (intent != null) {
			String title = intent.getStringExtra(Constants.NAME);
			String descrip = intent.getStringExtra(Constants.DESCRIPTION);
			((TextView) this.findViewById(R.id.gameDetails_textView_name)).setText(title);
			((TextView) this.findViewById(R.id.gameDetails_textView_description)).setText(descrip);
			((TextView) this.findViewById(R.id.gameDetails_textView_date)).append("10/29/2014");
			((TextView) this.findViewById(R.id.gameDetails_textView_playersJoined)).append("6");
			((TextView) this.findViewById(R.id.gameDetails_textView_playersNeeded)).append("10");
			((TextView) this.findViewById(R.id.gameDetails_textView_time)).append("6:30pm - 8:00pm");
			
			Button joinBtn = (Button) this.findViewById(R.id.gameDetails_button_join);
			joinBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v)
				{
					Intent i = new Intent(getApplicationContext(), HomeActivity.class);
					startActivity(i);
					finish();
				}
			});

			Button locationBtn = (Button) this.findViewById(R.id.gameDetails_button_map);
			locationBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v)
				{
					Intent i = new Intent(getApplicationContext(), GamesBrowseMapActivity.class);
					startActivity(i);
					finish();
				}
			});
		}
	}
}
